/*
 * LCD_display.c
 *
 *  Created on: Feb 17, 2021
 *      Author: DanWinkelstein
 */


#include "SC_des1.h"

#ifndef IPC_DISPLAY
extern XUartPs Uart_PS;		/* Instance of the UART Device */
#endif

// function prototypes for outputting to the LCD display
int LCD_output(char *write_buffer, int buffer_size);
void clear_LCD();
char StringLCD[100];

extern SC_DES1_T sc_des1_inst;

#define CLEAR_LCD_WAIT 1500

void display_function_select(){
	    //01234567890123456789012345678901234567890123456789012345678901234567890123456789
	strncpy(StringLCD,
		 "Function Sel:       1: VLT 4: ALM 7:MISC2: AMP 5: SHED      3: FRQ 6: RESTORE       ",80);
	clear_LCD();
	LCD_output(StringLCD, 80);
}

void display_volt(double volt_l1, double volt_l2, double volt_l3, double phasel1, double phasel2, double phasel3){
                          //01234567890123    45   6789012345678901234567890123456789012345678901234567890123456789
	                      //                           L1: mmm.dd/mmm.dd V L1: mmm.dd/mmm.dd V L1: mmm.dd/mmm.dd V
        snprintf(StringLCD, sizeof(StringLCD),"MAIN VOLT   A\x7F B\xB2   L1:% 06.2f/% 06.2fV L2:% 06.2f/% 06.2fV L3:% 06.2f/% 06.2fV    ",
        		volt_l1, VOLT208_calculation(volt_l1,volt_l2,phasel1),
        		volt_l2, VOLT208_calculation(volt_l2,volt_l3,phasel2),
        		volt_l3, VOLT208_calculation(volt_l3,volt_l1,phasel3));
    	clear_LCD();
    	LCD_output(StringLCD, 80);
}
void display_current_select(){
		//012345678901234   567   89012345678901234567890123456789012345678901234567890123456789
	strncpy(StringLCD,
		 "Current Sel: A\x7F B\xB2  1:MAIN 4: 40A 7: 20B2: 60A 5: 40B 8: BYP3: 60B 6: 20A          ",80);
	clear_LCD();
	LCD_output(StringLCD, 80);
}

void display_current(const char* feed, double amp_n, double amp_l1, double amp_l2, double amp_l3, double pf_l1, double pf_l2, double pf_l3){
                          //0123456789012345678   9012345678901234567890123456789012345678901234567890123456789
                          //        mmm.dd         L1: mmm.ddA PF m.dd L2: mmm.ddA PF m.dd L3: mmm.ddA PF m.dd
        snprintf(StringLCD, sizeof(StringLCD), ".... N:% 06.2fA  B\xB2  L1:% 06.2fA PF% 03.2f  L2:% 06.2fA PF% 03.2f  L3:% 06.2fA PF% 03.2f   ",
        		amp_n,amp_l1, pf_l1, amp_l2, pf_l2, amp_l3, pf_l3);
        strncpy(&StringLCD[0], feed, 4);
    	clear_LCD();
    	LCD_output(StringLCD, 80);
}


void display_freq(double freq_l1, double freq_l2, double freq_l3, double phase_l1, double phase_l2, double phase_l3){
                          //012345678901234   56   789012345678901234567   89012345678901234567   89012345678901234567   89
                          //                          L1: mm.ddHz mmm.d^     L2: mm.ddHz mmm.d^     L3: mm.ddHz mmm.d^
        snprintf(StringLCD, sizeof(StringLCD),"FREQ/PHASE:  B\xB2\x7E    L1:% 04.2fHz% 04.1f\xDF  L2:% 04.2fHz% 04.1f\xDF  L3:% 04.2fHz% 04.1f\xDF  ",
        		freq_l1, phase_l1, freq_l2, phase_l2, freq_l3, phase_l3);
    	clear_LCD();
    	LCD_output(StringLCD, 80);

}

void display_freq_setup(int freq_setting){
                          //012345678901234567   89012345678 901234567890123456789 012345678901234567890123456789
                          //
        snprintf(StringLCD,sizeof(StringLCD), "SET FREQUENCY:  B\xB2  1: 60Hz %c            2: 50HZ %c                               ",
        		(freq_setting == F60HZ ? '*':' '), (freq_setting == F50HZ ? ' ':'*'));
    	clear_LCD();
    	LCD_output(StringLCD, 80);


}



void display_alarms(){
	char ContactorStatus[4];
	// contactor display   0|60A|60B|40A|40B|20A|20B|BYP
	// circuit breaker     0|60A|60B|40A|40B|20A|20B|MAIN
	// contactor sense     FAIL|60A|60B|40A|40B|20A|20B|BYP
	if(sc_des1_inst.alarm_data->circuit_breaker_status_reg.bits.contactorOpen == 1 && sc_des1_inst.alarm_data->contactor_disengage_reg.reg == 0){
		strncpy(ContactorStatus,"Fail",4);
	} else {
		strncpy(ContactorStatus,"OK  ",4);
	}
                     //0123456789012   34567890123456789012345678        90123456789012345678       901234567890123456789
	snprintf(StringLCD,sizeof(StringLCD),"Alarms     B\xB2       Contactor %c%c%c%c%c%c%c%c  CB        %c%c%c%c%c%c%c%c  Cont Status %4s       ",
//			BYTE_TO_BINARY(sc_des1_inst.alarm_data->contactor_disengage_reg.reg),
			BYTE_TO_BINARY(sc_des1_inst.pca9539->port1.reg),
			BYTE_TO_BINARY_REVERSE(sc_des1_inst.alarm_data->circuit_breaker_status_reg.reg),
			ContactorStatus);
	clear_LCD();
   	LCD_output(StringLCD, 80);
}
void display_shed(){
	       //0123456789012   3456789012345678901234567890123456789012345678901234567890123456789
	strncpy(StringLCD,
			"SHED:      B\xB2              4: 40A 7: 20B2: 60A 5: 40B 8: TST3: 60B 6: 20A           ",80);
    clear_LCD();
    LCD_output(StringLCD, 80);
}

void display_restore() {
	       //0123456789012   3456789012345678901234567890123456789012345678901234567890123456789
	strncpy(StringLCD,
			"RESTORE:   B\xB2              4: 40A 7: 20B2: 60A 5: 40B 8: TST3: 60B 6: 20A         ",80);
    clear_LCD();
    LCD_output(StringLCD, 80);
}
void display_misc1(double temp, const char *ip_address, const char *controller, const char *device_id){
          //012345678901   23456789012345678901234567890123456789012345678901234567890123456789
	      //      mmm.1^           IP: 123.456.789.012 CTRL 123.456.789.012
   	snprintf(StringLCD,sizeof(StringLCD),
		   "temp: %4.1f\xDF         IP: %15s CTRL:%15sID %16s ",
			temp,ip_address,controller,device_id);
    clear_LCD();
    LCD_output(StringLCD, 80);

}
extern unsigned long long copyRealTimeDataCount;
void display_misc2(struct tm *tm){
// NOTE: added hidden commands to 3: turn dcBiasEnable ON, 4: turn dcBiasEnable OFF: no feedback to user
          //01234567  890  12345678901234567  890  123  4567890123456789012345678901234567890123456789
	unsigned long long totalBlocks = copyRealTimeDataCount * 256 / 36;
	snprintf(StringLCD,sizeof(StringLCD),
		   "Date: %02d/%02d/%04d    Time: %02d:%02d:%02d      1: LED_ON 2: LED_OFFCT:%16llu  ",
			tm->tm_mon, tm->tm_mday, tm->tm_year+1900, tm->tm_hour, tm->tm_min, tm->tm_sec, totalBlocks);  //number of blocks of data
    clear_LCD();
    LCD_output(StringLCD, 80);


}

#ifndef IPC_DISPLAY

int UartPsLCDinit(u16 DeviceID){
	int Status;
	XUartPs_Config *Config;

	/*
	 * Initialize the UART driver so that it's ready to use
	 * Look up the configuration in the config table and then initialize it.
	 */
	Config = XUartPs_LookupConfig(DeviceID);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	Status = XUartPs_CfgInitialize(&Uart_PS, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	XUartPs_SetBaudRate(&Uart_PS, 9600);

	/* Check hardware build. */
	Status = XUartPs_SelfTest(&Uart_PS);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	return XST_SUCCESS;

}

#ifndef ALTERNATE_LCD_OUTPUT
void clear_LCD(){
	char *clearScreen = "\xFE\x51";
	XUartPs_Send(&Uart_PS, (u8 *) clearScreen, (u32) 2);
	usleep(CLEAR_LCD_WAIT);  // sleep 1.5ms after clearing screen
}
#else
void clear_LCD(){
	//no operation
}
#endif

#define MAX(a,b) (a>b?a:b)
#define MIN(a,b) (a<b?a:b)
//#define DISABLE_LCD

#ifndef ALTERNATE_LCD_OUTPUT
int LCD_output(char write_buffer[], int buffer_size){
	/* Block sending the buffer. */
	u8 *setCursorLine0 = (u8 *) "\xFE\x45\x00";
	u8 *setCursorLine1 = (u8 *) "\xFE\x45\x40";
	u8 *setCursorLine2 = (u8 *) "\xFE\x45\x14";
	u8 *setCursorLine3 = (u8 *) "\xFE\x45\x54";
	int buffer_offset = 0;
	u32 SentCount;

	if(buffer_size > 80) buffer_size = 80;
	while(buffer_size > 0){
		if(buffer_offset < 20) {
			SentCount = XUartPs_Send(&Uart_PS, (u8 *) setCursorLine0, (u32) 3);
		} else if(buffer_offset < 40){
			SentCount = XUartPs_Send(&Uart_PS, (u8 *) setCursorLine1, (u32) 3);
		} else if(buffer_offset < 60) {
			SentCount = XUartPs_Send(&Uart_PS, (u8 *) setCursorLine2, (u32) 3);
		} else {
			SentCount = XUartPs_Send(&Uart_PS, (u8 *) setCursorLine3, (u32) 3);
		}
		usleep(10000);
		for(int i = 0; i < MIN(20,buffer_size); i++) {
			u32 SentCount = XUartPs_Send(&Uart_PS, (u8 *) &write_buffer[buffer_offset + i], (u32) 1);
			usleep(1000);
		}
		buffer_size -= MIN(20,buffer_size);
		buffer_offset += 20;
	}
//	SentCount = XUartPs_Send(&Uart_PS, (u8 *) write_buffer, (u32) buffer_size);
//	if (SentCount != buffer_size) {
//		return XST_FAILURE;
//	}
	return SentCount;
}
#else
int LCD_output(char write_buffer[], int buffer_size){
#ifdef DISABLE_LCD
	return 0;
#else
	char output_buffer[80];
	u32 SentCount = 0;
	memset(output_buffer,' ',80);  // fill with <space> character
	/* Block sending the buffer. */
	u8 *setCursorLine0 = (u8 *) "\xFE\x45\x00";
	SentCount = XUartPs_Send(&Uart_PS, (u8 *) setCursorLine0, (u32) 3);
	usleep(5000);
	memcpy(&output_buffer[0],&write_buffer[0],MIN(20,buffer_size));  //copy the first 20 bytes of the write buffer to the output buffer
	if(buffer_size > 20) {
	     memcpy(&output_buffer[40],&write_buffer[20],MIN(40,buffer_size) - 20);  //copy the second 20 bytes of the write buffer to the output buffer at location 40
	}
	if(buffer_size > 40) {
	     memcpy(&output_buffer[20],&write_buffer[40],MIN(60,buffer_size) - 40);  //copy the third 20 bytes of the write buffer to the output buffer at location 20
	}
	if(buffer_size > 60) {
	     memcpy(&output_buffer[60],&write_buffer[60],MIN(80,buffer_size) - 60);  //copy the forth 20 bytes of the write buffer to the output buffer at location 60
	}
	for(int i = 0; i < MIN(80,buffer_size); i++) {
		SentCount += XUartPs_Send(&Uart_PS, (u8 *) &output_buffer[i], (u32) 1);
		usleep(200);
	}
	return SentCount;
#endif
}

#endif


void display_off() {
	char *displayOff = "\xFE\x53\x01";
	XUartPs_Send(&Uart_PS, (u8 *) displayOff, (u32) 3);
	usleep(100);  // sleep 1.5ms after clearing screen
//	*displayOff = "\xFE\x42";
//	XUartPs_Send(&Uart_PS, (u8 *) displayOff, (u32) 2);
//	usleep(100);  // sleep 1.5ms after clearing screen
}

void display_on() {
//	char *displayOn = "\xFE\x41";
//	XUartPs_Send(&Uart_PS, (u8 *) displayOn, (u32) 2);
//	usleep(100);  // sleep 1.5ms after clearing screen
	char *displayOn = "\xFE\x53\x08";
	XUartPs_Send(&Uart_PS, (u8 *) displayOn, (u32) 3);
	usleep(100);  // sleep 1.5ms after clearing screen
}

void set_display_baud(){
	//changes the display baud rate to 115200 baud
	char *displayBaud = "\xFE\x61\x08";
	XUartPs_Send(&Uart_PS, (u8 *) displayBaud, (u32) 3);
	usleep(3000);  // sleep 3ms after changing baud
	XUartPs_SetBaudRate(&Uart_PS, 115200);
	usleep(3000);  // sleep 3ms after changing baud
}
#else //IPC_DISPLAY
IPC_INTERRUPT_T IPC_interrupt_reg;
void clear_LCD() {
	do{
		IPC_interrupt_reg.reg = Xil_In32(IPC_INTERRUPT_ADDR);
	} 	while(IPC_interrupt_reg.bits.IPC_interrupt == 1);
	Xil_Out32(IPC_COMMAND_ADDR, IPC_COMMAND_CLEAR);
	Xil_Out32(IPC_INTERRUPT_ADDR, 1);
}

void display_off() {
	do{
		IPC_interrupt_reg.reg = Xil_In32(IPC_INTERRUPT_ADDR);
	} 	while(IPC_interrupt_reg.bits.IPC_interrupt == 1);
	Xil_Out32(IPC_COMMAND_ADDR, IPC_COMMAND_LCD_OFF);
	Xil_Out32(IPC_INTERRUPT_ADDR, 1);
}

void display_on() {
	do{
		IPC_interrupt_reg.reg = Xil_In32(IPC_INTERRUPT_ADDR);
	} 	while(IPC_interrupt_reg.bits.IPC_interrupt == 1);
	Xil_Out32(IPC_COMMAND_ADDR, IPC_COMMAND_LCD_ON);
	Xil_Out32(IPC_INTERRUPT_ADDR, 1);
}

int LCD_output(char write_buffer[], int buffer_size){
	do{
		IPC_interrupt_reg.reg = Xil_In32(IPC_INTERRUPT_ADDR);
	} 	while(IPC_interrupt_reg.bits.IPC_interrupt == 1);
	int transfer_size = buffer_size > 80 ? 80 : buffer_size;
	Xil_MemCpy(IPC_DATA0_ADDR, write_buffer, transfer_size);
	Xil_Out32(IPC_LENGTH_ADDR, transfer_size);
	Xil_Out32(IPC_COMMAND_ADDR, IPC_COMMAND_OUTPUT);
	Xil_Out32(IPC_INTERRUPT_ADDR, 1);
	return transfer_size;
}

#endif
