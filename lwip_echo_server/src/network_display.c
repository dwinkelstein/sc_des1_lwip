/*
 * network_display.c
 *
 *  Created on: Apr 20, 2021
 *      Author: DanWinkelstein
 */

#include "SC_des1.h"
extern SC_DES1_T sc_des1_inst;
extern unsigned long long copyRealTimeDataCount;


int network_response_display(struct pbuf *p, char *response_string) {
	char request[p->len];
	strncpy(request, (const char *)p->payload, p->len);


	double volt_l1 = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_volt_l1, sc_des1_inst.sensor_factor->volt_factor_l1);
	double volt_l2 = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_volt_l2, sc_des1_inst.sensor_factor->volt_factor_l2);
	double volt_l3 = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_volt_l3, sc_des1_inst.sensor_factor->volt_factor_l3);

	if(request[0] == 'v'){
		double phasel1 = PHASE_calculation(sc_des1_inst.alarm_data->main_l1_freq_reg, sc_des1_inst.alarm_data->main_l1_phase_reg);
		double phasel2 = PHASE_calculation(sc_des1_inst.alarm_data->main_l2_freq_reg, sc_des1_inst.alarm_data->main_l2_phase_reg);
		double phasel3 = PHASE_calculation(sc_des1_inst.alarm_data->main_l3_freq_reg, sc_des1_inst.alarm_data->main_l3_phase_reg);
		double volt_l1_l2 = VOLT208_calculation(volt_l1,volt_l2,phasel1);
		double volt_l2_l3 = VOLT208_calculation(volt_l2,volt_l3,phasel2);
		double volt_l3_l1 = VOLT208_calculation(volt_l3,volt_l1,phasel3);
		sprintf(response_string, "\r\nMAIN Voltage: L1 %6.2f/%6.2f V  L2 %6.2f/%6.2f V L3 %6.2f/%6.2f V\r\n", volt_l1, volt_l1_l2, volt_l2, volt_l2_l3, volt_l3, volt_l3_l1);
	} else if(request[0] == '1'){
		double amp_n = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_main_n, sc_des1_inst.sensor_factor->current_main_factor_n);
		double amp_l1 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_main_l1, sc_des1_inst.sensor_factor->current_main_factor_l1);
		double amp_l2 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_main_l2, sc_des1_inst.sensor_factor->current_main_factor_l2);
		double amp_l3 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_main_l3, sc_des1_inst.sensor_factor->current_main_factor_l3);
		double pf_l1 =  PF_calculation(volt_l1, amp_l1, sc_des1_inst.snapshot_data->snapshot_pow_main_l1, sc_des1_inst.sensor_factor->volt_factor_l1, sc_des1_inst.sensor_factor->current_main_factor_l1);
		double pf_l2 =  PF_calculation(volt_l2, amp_l2, sc_des1_inst.snapshot_data->snapshot_pow_main_l2, sc_des1_inst.sensor_factor->volt_factor_l2, sc_des1_inst.sensor_factor->current_main_factor_l2);
		double pf_l3 =  PF_calculation(volt_l3, amp_l3, sc_des1_inst.snapshot_data->snapshot_pow_main_l3, sc_des1_inst.sensor_factor->volt_factor_l3, sc_des1_inst.sensor_factor->current_main_factor_l3);
		sprintf(response_string, "\r\nMAIN Current: L1 %6.2f A, PF %3.2f   L2 %6.2f A, PF %3.2f  L3 %6.2f A, PF %3.2f  N %6.2f A\r\n", amp_l1, pf_l1, amp_l2, pf_l2, amp_l3, pf_l3, amp_n);
	} else if(request[0] == '2'){
		double amp_n = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60a_n, sc_des1_inst.sensor_factor->current_60A_factor_n);
		double amp_l1 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60a_l1, sc_des1_inst.sensor_factor->current_60A_factor_l1);
		double amp_l2 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60a_l2, sc_des1_inst.sensor_factor->current_60A_factor_l2);
		double amp_l3 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60a_l3, sc_des1_inst.sensor_factor->current_60A_factor_l3);
		double pf_l1 =  PF_calculation(volt_l1, amp_l1, sc_des1_inst.snapshot_data->snapshot_pow_f60a_l1, sc_des1_inst.sensor_factor->volt_factor_l1, sc_des1_inst.sensor_factor->current_60A_factor_l1);
		double pf_l2 =  PF_calculation(volt_l2, amp_l2, sc_des1_inst.snapshot_data->snapshot_pow_f60a_l2, sc_des1_inst.sensor_factor->volt_factor_l2, sc_des1_inst.sensor_factor->current_60A_factor_l2);
		double pf_l3 =  PF_calculation(volt_l3, amp_l3, sc_des1_inst.snapshot_data->snapshot_pow_f60a_l3, sc_des1_inst.sensor_factor->volt_factor_l3, sc_des1_inst.sensor_factor->current_60A_factor_l3);
		sprintf(response_string, "\r\nF60A Current: L1 %6.2f A, PF %3.2f   L2 %6.2f A, PF %3.2f  L3 %6.2f A, PF %3.2f  N %6.2f A\r\n", amp_l1, pf_l1, amp_l2, pf_l2, amp_l3, pf_l3, amp_n);
	} else if(request[0] == '3'){
		double amp_n = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60b_n, sc_des1_inst.sensor_factor->current_60B_factor_n);
		double amp_l1 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60b_l1, sc_des1_inst.sensor_factor->current_60B_factor_l1);
		double amp_l2 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60b_l2, sc_des1_inst.sensor_factor->current_60B_factor_l2);
		double amp_l3 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60b_l3, sc_des1_inst.sensor_factor->current_60B_factor_l3);
		double pf_l1 =  PF_calculation(volt_l1, amp_l1, sc_des1_inst.snapshot_data->snapshot_pow_f60b_l1, sc_des1_inst.sensor_factor->volt_factor_l1, sc_des1_inst.sensor_factor->current_60B_factor_l1);
		double pf_l2 =  PF_calculation(volt_l2, amp_l2, sc_des1_inst.snapshot_data->snapshot_pow_f60b_l2, sc_des1_inst.sensor_factor->volt_factor_l2, sc_des1_inst.sensor_factor->current_60B_factor_l2);
		double pf_l3 =  PF_calculation(volt_l3, amp_l3, sc_des1_inst.snapshot_data->snapshot_pow_f60b_l3, sc_des1_inst.sensor_factor->volt_factor_l3, sc_des1_inst.sensor_factor->current_60B_factor_l3);
		sprintf(response_string, "\r\nF60b Current: L1 %6.2f A, PF %3.2f   L2 %6.2f A, PF %3.2f  L3 %6.2f A, PF %3.2f  N %6.2f A\r\n", amp_l1, pf_l1, amp_l2, pf_l2, amp_l3, pf_l3, amp_n);
	} else if(request[0] == '4'){
		double amp_n = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40a_n, sc_des1_inst.sensor_factor->current_40A_factor_n);
		double amp_l1 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40a_l1, sc_des1_inst.sensor_factor->current_40A_factor_l1);
		double amp_l2 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40a_l2, sc_des1_inst.sensor_factor->current_40A_factor_l2);
		double amp_l3 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40a_l3, sc_des1_inst.sensor_factor->current_40A_factor_l3);
		double pf_l1 =  PF_calculation(volt_l1, amp_l1, sc_des1_inst.snapshot_data->snapshot_pow_f40a_l1, sc_des1_inst.sensor_factor->volt_factor_l1, sc_des1_inst.sensor_factor->current_40A_factor_l1);
		double pf_l2 =  PF_calculation(volt_l2, amp_l2, sc_des1_inst.snapshot_data->snapshot_pow_f40a_l2, sc_des1_inst.sensor_factor->volt_factor_l2, sc_des1_inst.sensor_factor->current_40A_factor_l2);
		double pf_l3 =  PF_calculation(volt_l3, amp_l3, sc_des1_inst.snapshot_data->snapshot_pow_f40a_l3, sc_des1_inst.sensor_factor->volt_factor_l3, sc_des1_inst.sensor_factor->current_40A_factor_l3);
		sprintf(response_string, "\r\nF40a Current: L1 %6.2f A, PF %3.2f   L2 %6.2f A, PF %3.2f  L3 %6.2f A, PF %3.2f  N %6.2f A\r\n", amp_l1, pf_l1, amp_l2, pf_l2, amp_l3, pf_l3, amp_n);
	} else if(request[0] == '5'){
		double amp_n = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40b_n, sc_des1_inst.sensor_factor->current_40B_factor_n);
		double amp_l1 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40b_l1, sc_des1_inst.sensor_factor->current_40B_factor_l1);
		double amp_l2 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40b_l2, sc_des1_inst.sensor_factor->current_40B_factor_l2);
		double amp_l3 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40b_l3, sc_des1_inst.sensor_factor->current_40B_factor_l3);
		double pf_l1 =  PF_calculation(volt_l1, amp_l1, sc_des1_inst.snapshot_data->snapshot_pow_f40b_l1, sc_des1_inst.sensor_factor->volt_factor_l1, sc_des1_inst.sensor_factor->current_40B_factor_l1);
		double pf_l2 =  PF_calculation(volt_l2, amp_l2, sc_des1_inst.snapshot_data->snapshot_pow_f40b_l2, sc_des1_inst.sensor_factor->volt_factor_l2, sc_des1_inst.sensor_factor->current_40B_factor_l2);
		double pf_l3 =  PF_calculation(volt_l3, amp_l3, sc_des1_inst.snapshot_data->snapshot_pow_f40b_l3, sc_des1_inst.sensor_factor->volt_factor_l3, sc_des1_inst.sensor_factor->current_40B_factor_l3);
		sprintf(response_string, "\r\nF40b Current: L1 %6.2f A, PF %3.2f   L2 %6.2f A, PF %3.2f  L3 %6.2f A, PF %3.2f  N %6.2f A\r\n", amp_l1, pf_l1, amp_l2, pf_l2, amp_l3, pf_l3, amp_n);
	} else if(request[0] == '6'){
		double amp_n = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f20a_n, sc_des1_inst.sensor_factor->current_20A_factor_n);
		double amp_l1 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f20a_l1, sc_des1_inst.sensor_factor->current_20A_factor_l1);
		double pf_l1 =  PF_calculation(volt_l1, amp_l1, sc_des1_inst.snapshot_data->snapshot_pow_f20a_l1, sc_des1_inst.sensor_factor->volt_factor_l1, sc_des1_inst.sensor_factor->current_20A_factor_l1);
		sprintf(response_string, "\r\nF20a Current: L1 %6.2f A, PF %3.2f   N %6.2f A\r\n", amp_l1, pf_l1, amp_n);
	} else if(request[0] == '7'){
		double amp_n = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f20b_n, sc_des1_inst.sensor_factor->current_20B_factor_n);
		double amp_l2 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f20b_l2, sc_des1_inst.sensor_factor->current_20B_factor_l2);
		double pf_l2 =  PF_calculation(volt_l2, amp_l2, sc_des1_inst.snapshot_data->snapshot_pow_f20b_l2, sc_des1_inst.sensor_factor->volt_factor_l2, sc_des1_inst.sensor_factor->current_20B_factor_l2);
		sprintf(response_string, "\r\nF20b Current:   L2 %6.2f A, PF %3.2f  N %6.2f A\r\n",  amp_l2, pf_l2,  amp_n);
	} else if(request[0] == '8'){
		double amp_n = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_byp_n, sc_des1_inst.sensor_factor->current_BYP_factor_n);
		double amp_l1 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_byp_l1, sc_des1_inst.sensor_factor->current_BYP_factor_l1);
		double amp_l2 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_byp_l2, sc_des1_inst.sensor_factor->current_BYP_factor_l2);
		double amp_l3 =  RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_byp_l3, sc_des1_inst.sensor_factor->current_BYP_factor_l3);
		double pf_l1 =  PF_calculation(volt_l1, amp_l1, sc_des1_inst.snapshot_data->snapshot_pow_byp_l1, sc_des1_inst.sensor_factor->volt_factor_l1, sc_des1_inst.sensor_factor->current_BYP_factor_l1);
		double pf_l2 =  PF_calculation(volt_l2, amp_l2, sc_des1_inst.snapshot_data->snapshot_pow_byp_l2, sc_des1_inst.sensor_factor->volt_factor_l2, sc_des1_inst.sensor_factor->current_BYP_factor_l2);
		double pf_l3 =  PF_calculation(volt_l3, amp_l3, sc_des1_inst.snapshot_data->snapshot_pow_byp_l3, sc_des1_inst.sensor_factor->volt_factor_l3, sc_des1_inst.sensor_factor->current_BYP_factor_l3);
		sprintf(response_string, "\r\nBypass Current: L1 %6.2f A, PF %3.2f   L2 %6.2f A, PF %3.2f  L3 %6.2f A, PF %3.2f  N %6.2f A\r\n", amp_l1, pf_l1, amp_l2, pf_l2, amp_l3, pf_l3, amp_n);
	} else if(request[0] == 'f'){
		double freql1 = FREQ_calculation(sc_des1_inst.alarm_data->main_l1_freq_reg);
		double freql2 = FREQ_calculation(sc_des1_inst.alarm_data->main_l2_freq_reg);
		double freql3 = FREQ_calculation(sc_des1_inst.alarm_data->main_l3_freq_reg);
		double phasel1 = PHASE_calculation(sc_des1_inst.alarm_data->main_l1_freq_reg, sc_des1_inst.alarm_data->main_l1_phase_reg);
		double phasel2 = PHASE_calculation(sc_des1_inst.alarm_data->main_l2_freq_reg, sc_des1_inst.alarm_data->main_l2_phase_reg);
		double phasel3 = PHASE_calculation(sc_des1_inst.alarm_data->main_l3_freq_reg, sc_des1_inst.alarm_data->main_l3_phase_reg);
		sprintf(response_string, "\r\nFrequency L1 %6.2f Phase L1/L2 %6.1f  Frequency L2 %6.2f Phase L2/L3 %6.1f  Frequency L3 %6.2f Phase L3/L1 %6.1f \r\n", freql1, phasel1, freql2, phasel2, freql3, phasel3);
	} else if(request[0] == 'a'){
		char ContactorStatus[4];
		if(sc_des1_inst.alarm_data->circuit_breaker_status_reg.bits.contactorOpen == 1 && sc_des1_inst.alarm_data->contactor_disengage_reg.reg == 0){
			strncpy(ContactorStatus,"Fail",4);
		} else {
			strncpy(ContactorStatus,"OK  ",4);
		}
		sprintf(response_string,"\r\nAlarms \r\n Contactor %c%c%c%c%c%c%c%c \r\n Circuit Breaker %c%c%c%c%c%c%c%c \r\n Contactor Status Status %4s  \r\n",
				BYTE_TO_BINARY(sc_des1_inst.alarm_data->contactor_disengage_reg.reg),
				BYTE_TO_BINARY(sc_des1_inst.alarm_data->circuit_breaker_status_reg.reg),
				ContactorStatus);
	} else if(request[0] == 's'){
		sprintf(response_string,"\r\nAll circuits SHED\r\n");
		CONTACTOR_DISENGAGE_T contactor_setting;
		contactor_setting.reg = 0x7F;
		set_contactor(contactor_setting);
	} else if(request[0] == 'r'){
		sprintf(response_string,"\r\nAll circuits RESTORED\r\n");
		CONTACTOR_DISENGAGE_T contactor_setting;
		contactor_setting.reg = 0;
		set_contactor(contactor_setting);
	} else if(request[0] == 't'){
		sprintf(response_string, "\r\nTemperature %4.1f C\r\n", sc_des1_inst.temperature);
	} else if(request[0] == 'p'){
		sprintf(response_string,"Realtime data block count %llu\r\n",copyRealTimeDataCount);
	} else {
	strcpy(response_string,"\r\n v: voltage\r\n 1: current MAIN\r\n 2: current F60A\r\n 3: current F60B\r\n 4: current F40A\r\n 5: current F40B\r\n 6: current F20A\r\n 7: current F20B\r\n 8: current BYP\r\n f: frequency/phase\r\n a: alarms\r\n s: shed\r\n r: restore\r\n t: temperature\r\n p: sample blocks\r\n");
	}
	return strlen(response_string);

}
