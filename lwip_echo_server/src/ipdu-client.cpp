/*
 * ipdu-client.cpp
 *
 *  Created on: Apr 26, 2021
 *      Author: DanWinkelstein
 */

/*
 * Copyright (c) 2021 Aura Technologies, LLC
 *
 */


//#include <sys/types.h>
////#include <sys/socket.h>
////#include <netinet/in.h>
////#include <arpa/inet.h>
////#include <netdb.h>
//#include <unistd.h>
#include <errno.h>
//#include <ctype.h>
//#include <stdio.h>
#include <string.h>
//#include <stdlib.h>
////#include <sys/epoll.h>
//#include <fcntl.h>
#include <string>
#include <map>
#include "SC_des1.h"
//
//#include "testdata.h"
//#include "global.h"
//
//#include "lwip/opt.h"
//
//#include "lwip/tcpbase.h"
//#include "lwip/mem.h"
//#include "lwip/pbuf.h"
//#include "lwip/ip.h"
//#include "lwip/icmp.h"
//#include "lwip/err.h"
//#include "lwip/ip6.h"
//#include "lwip/ip6_addr.h"
//#include "lwip/tcp.h"
//#include "lwip/inet.h"
//#include <math.h>

extern SC_DES1_T sc_des1_inst;

// ************************* Local Globals ***************************
//
// Version #
const char *Version = "0.000.005";

// command/response message map
static std::map<std::string, enum messageIdentifier> s_messageMap;

serverConnection     currentServer      = {0};
newAlarmLimits       updatedAlarmLimits = {0};     // holds new limits sent by controller

unsigned char        outputBuffer[4096];
unsigned char        readInBuffer[4096];

bool  timeToQuit = false;

extern u32 realTimeDataA[EIGHT_SECONDS_DATA];
extern u32 realTimeDataB[EIGHT_SECONDS_DATA];
extern u32 realTimeDataC[EIGHT_SECONDS_DATA];
extern int currentBuffer;
extern alarmData  testAlarmData;

struct sendMsgQueueElement sendMsgQueue[SEND_MSG_QUEUE_LEN];
int enqueueIndex = 0;
int dequeueIndex = 0;

static int outputMessage( void *outputBuffer, serverConnection *currentServer, int byteCount );
static int sendMessage( void *outputBuffer, serverConnection *currentServer, int byteCount );

// ************************ Prototypes ***************************************
//
//extern int responseParser( enum messageIdentifier newOutputMsg,
//                           char msgDataByte,
//                           serverConnection *currentServer );

//*****************************************************************************
// Function:   initMessageMapping
//
// Initialize the message map creating a simple string/value mapping function.
// This used to make other processes work better.
//
// Input:
// Output:  s_messageMap - initialized
//*****************************************************************************
//
static void initMessageMapping ( void )
{
   s_messageMap["AK"] = ACK_MSG;
   s_messageMap["FS"] = FEED_SHED_PRIORITY_MSG;
   s_messageMap["HB"] = HB_MSG;
   s_messageMap["GA"] = GET_ALARMS_MSG;
   s_messageMap["GC"] = GET_CONFIG_MSG;
   s_messageMap["GR"] = GET_REALTIME_DATA_MSG;
   s_messageMap["GS"] = GET_SNAPSHOT_DATA_MSG;
   s_messageMap["GT"] = GET_TEMPERATURE_MSG;
   s_messageMap["RT"] = RESPONSE_TEMPERATURE_MSG;
   s_messageMap["ND"] = NO_DATA_MSG;
   s_messageMap["NK"] = NAK_MSG;
   s_messageMap["RA"] = ALARM_RESPONSE_MSG;
   s_messageMap["RC"] = CONFIG_UPDATE_MSG;
   s_messageMap["RS"] = SNAPSHOT_DATA_MSG;
   s_messageMap["RR"] = REALTIME_DATA_MSG;
   s_messageMap["SA"] = SET_ALARM_LIMITS_MSG;
   s_messageMap["SC"] = SET_CONFIG_MSG;
   s_messageMap["SE"] = SET_ENABLE_MSG;
   s_messageMap["SF"] = SET_INPUT_FREQUENCY_MSG;
   s_messageMap["SL"] = SET_LEDS_MSG;
   s_messageMap["SM"] = SET_MONITORING_MSG;
   s_messageMap["SP"] = SET_FEED_CONTROL_MSG;
   s_messageMap["SS"] = SET_STREAMING_MSG;
   s_messageMap["ST"] = SET_TIME_MSG;
   s_messageMap["IL"] = ILLEGAL_MSG;
   s_messageMap["GN"] = GET_NODATA_MSG;
}

//*****************************************************************************
// Function:   initialization
//
// This function performes all the necessary environment initialization.
//
// Input:
// Output:
//*****************************************************************************
//
void initialization ( void )
{
#ifdef DEBUG
   printf("Aura Technologies, LLC\n");
   printf("TacPwr IPDU Client %s\n\n", Version);
#endif

   initMessageMapping();               // create message map

   // initialize main client data structure
   currentServer.deviceID                    = (long long) sc_des1_inst.device_id;
   currentServer.fd                          = 0;
   currentServer.messageID                   = 0;
   currentServer.state                       = UNKNOWN;
   currentServer.expectedResponse            = NO_MSG;
   currentServer.realtimeBlocksCount         = 0;
   currentServer.controlReg                  = 205;
   currentServer.contactorState              = 77;
   currentServer.currentAlarmData            = &testAlarmData;
   currentServer.currentAlarmLimits          = &testAlarmLimits;
   currentServer.currentFeedShedPriorities   = &testFeedShedPriority;
   currentServer.currentCorrectionFactors    = &testCorrectionFactors;
   currentServer.currentSnapshotData         = &testSnapshotData;
   currentServer.currentRealtimeData         = testRealtimeData;
}

#if 0
//*****************************************************************************
// Function:   createSocket
//
// Open new socket and bind to current address and port.
//
// Input:   char *address
//          char *port - port number
// Output:  -1 failure, otherwise fd for the socket
//*****************************************************************************
//
static int createSocket( char *address, char *port )
{
   int   socketPeer;
   char  addressBuffer[NI_MAXHOST];
   char  serviceBuffer[NI_MAXSERV];

#ifdef DEBUG
   printf( "Configuring local address...\n" );
#endif

   struct addrinfo hints;
   memset( &hints, 0, sizeof(hints) );
   hints.ai_family   = AF_INET;
   hints.ai_socktype = SOCK_STREAM;
   hints.ai_flags    = AI_PASSIVE;

   struct addrinfo *peerAddress;
   if ( getaddrinfo( address, port, &hints, &peerAddress ) )
   {
      fprintf( stderr, "getaddrinfo() failed. (%d)\n", errno );
      return -1;
   }

#ifdef DEBUG
   printf( "Remote address is: " );
#endif
   getnameinfo( peerAddress->ai_addr, peerAddress->ai_addrlen,
                addressBuffer, sizeof(addressBuffer),
                serviceBuffer, sizeof(serviceBuffer),
                NI_NUMERICHOST );
#ifdef DEBUG
   printf( "%s %s\n", addressBuffer, serviceBuffer );
#endif


#ifdef DEBUG
   printf( "Creating socket...\n" );
#endif

   socketPeer = socket( peerAddress->ai_family,
                        peerAddress->ai_socktype,
                        peerAddress->ai_protocol );
   if ( !ISVALIDSOCKET( socketPeer ) )
   {
      fprintf( stderr, "socket() failed. (%d)\n", errno );
      return -1;
   }

#ifdef DEBUG
   printf( "Connecting...\n" );
#endif
   if ( connect( socketPeer, peerAddress->ai_addr, peerAddress->ai_addrlen ) )
   {
      fprintf( stderr, "connect() failed. (%d)\n", errno );
      return -1;
   }
   freeaddrinfo( peerAddress );

   return socketPeer;
}


//*****************************************************************************
// Function:   makeSocketNonBlocking
//
// Modify the socket attributes so it becomes Non-Blocking
//
// Input:   int - file descriptor for the socket
// Output:  int - success/failure 0/-1
//*****************************************************************************
//
static int makeSocketNonBlocking ( int nfd )
{
   int   flags, s;

   flags = fcntl ( nfd, F_GETFL, 0 );
   if ( flags == -1 )
   {
      perror ( "fcntl" );
      return -1;
   }

   flags |= O_NONBLOCK;
   if ( (fcntl ( nfd, F_SETFL, flags )) == -1 )
   {
      perror ( "fcntl" );
      return -1;
   }
   return 0;
}

#endif
//*****************************************************************************
// Function:   getConfigData
//
// Marshal the data needed to report the current configuration of the client to the server.
//
// Input:   output message structure, destination storage address, server connection data
// Output:
//*****************************************************************************
//
static void getConfigData( MessageStruct *outputMsg,
                          unsigned char *destAddress,
                          serverConnection *currentServer )
{
   int  i = 0;
   unsigned char *sourceAddress = 0;

   // determine the size of the payload
   // Control state + Alarm Limits + Correction Factors + Feed Shed Priorities + time stamp
   int controlSize      =   (2 * sizeof( long ));                             // control state data
   int alarmSize        =   (char *)&testAlarmLimits.endOfStruct              // alarm limits
                          - (char *)&testAlarmLimits.main_l1_volt_max;
   int correctionSize   =   (char *)&testCorrectionFactors.endOfStruct        // correction factors
                          - (char *)&testCorrectionFactors.voltage_factor;
   int prioritySize     =   (char *)&testFeedShedPriority.endOfStruct         // feed shed priorities
                          - (char *)&testFeedShedPriority.main_60A_priority;
   int timeSize         =   (char *)&testTimeValue.endOfStruct                // time stamp data
                          - (char *)&testTimeValue.timeHigh;

   // transfer payload size
   sourceAddress         = (unsigned char *)&outputMsg->dataLength;
   outputMsg->dataLength =   controlSize
                           + alarmSize
                           + correctionSize
                           + prioritySize
                           + timeSize;

//   // transfer payload data length value
//   for ( i = 0; i < sizeof(int); i++ )
//   {
//      *destAddress++ = *sourceAddress++;
//   }
   // move the length of the data to the output buffer
   Xil_MemCpy(destAddress, sourceAddress, sizeof(int));
   destAddress += sizeof(int);

//   // transfer Control State
//   sourceAddress  = (unsigned char *)&currentServer->controlReg;
//   for ( i = 0; i < controlSize; i++ )
//   {
//      *destAddress++ = *sourceAddress++;
//   }
   Xil_MemCpy(destAddress, &sc_des1_inst.alarm_data->control_reg, sizeof(CONTROL_REG_T));
   destAddress += sizeof(CONTROL_REG_T);

//   // transfer Alarm Limits
//   sourceAddress  = (unsigned char *)&testAlarmLimits.main_l1_volt_max;
//   for ( i = 0; i < alarmSize; i++ )
//   {
//      *destAddress++ = *sourceAddress++;
//   }
   Xil_MemCpy(destAddress, &sc_des1_inst.alarm_data->main_l1_volt_max_reg, sizeof(ALARM_LIMIT_T));
   destAddress += sizeof(ALARM_LIMIT_T);


//   // transfer Correction Factors
//   sourceAddress  = (unsigned char *)&testCorrectionFactors.voltage_factor;
//   for ( i = 0; i < correctionSize; i++ )
//   {
//      *destAddress++ = *sourceAddress++;
//   }
   Xil_MemCpy(destAddress, &sc_des1_inst.sensor_factor->volt_factor_l1, sizeof(SENSOR_FACTOR_T));
   destAddress += sizeof(SENSOR_FACTOR_T);

   // transfer Feed Shed Priorities
   // DUMMY NUMBERS since this is not used in embedded currently
   sourceAddress  = (unsigned char *)&testFeedShedPriority.main_60A_priority;
   for ( i = 0; i < prioritySize; i++ )
   {
      *destAddress++ = *sourceAddress++;
   }

//   // transfer Time Stamp
//   sourceAddress  = (unsigned char *)&testTimeValue.timeHigh;
//   for ( i = 0; i < timeSize; i++ )
//   {
//      *destAddress++ = *sourceAddress++;
//   }
   Xil_MemCpy(destAddress, &sc_des1_inst.alarm_data->timestamp_seconds_msb, sizeof(TIMESTAMP_REGISTER_T));
   destAddress += sizeof(TIMESTAMP_REGISTER_T);

}

//*****************************************************************************
// Function:   getSnapshotData
//
// Marshal the Snapshot data for sending to the server.
//
// Input:   output message structure, destination storage address, server connection data
// Output:
//*****************************************************************************
//
static void getSnapshotData( MessageStruct *outputMsg,
                             unsigned char *destAddress,
                             serverConnection *currentServer )
{
   int  i              = 0;
   int  j              = 0;
   char *sourceAddress = 0;

   // get size of the snapshot data + sincle cycle data sets
   int  snapshotSize   =   (char *)&testSnapshotData.endSnapData
                         - (char *)&testSnapshotData.controlReg;
   int  cycleSize      =   (char *)&testSnapshotData.cycleData[0].endOfBlock
                         - (char *)&testSnapshotData.cycleData[0].main_l1_volt_reg;

   // calculate and transfer payload size
   sourceAddress         = (char *)&outputMsg->dataLength;
   //   outputMsg->dataLength = snapshotSize + (8 * cycleSize);
      outputMsg->dataLength = snapshotSize ;

   // transfer payload data length value
   for ( i = 0; i < sizeof(int); i++ )
   {
      *destAddress++ = *sourceAddress++;
   }

#ifdef DEBUG
   printf("Snapshot Data size %d\n", outputMsg->dataLength );
#endif

   testSnapshotData.controlReg = sc_des1_inst.alarm_data->control_reg.reg;
   testSnapshotData.contactorState = sc_des1_inst.alarm_data->contactor_disengage_reg.reg;
   testSnapshotData.main_l1_volt = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_volt_l1, sc_des1_inst.sensor_factor->volt_factor_l1);
   testSnapshotData.main_l2_volt = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_volt_l2, sc_des1_inst.sensor_factor->volt_factor_l2);
   testSnapshotData.main_l3_volt = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_volt_l3, sc_des1_inst.sensor_factor->volt_factor_l3);
   testSnapshotData.main_l1_freq = FREQ_calculation(sc_des1_inst.alarm_data->main_l1_freq_reg);
   testSnapshotData.main_l2_freq = FREQ_calculation(sc_des1_inst.alarm_data->main_l2_freq_reg);
   testSnapshotData.main_l3_freq = FREQ_calculation(sc_des1_inst.alarm_data->main_l3_freq_reg);
   testSnapshotData.main_l1_l2_phase = PHASE_calculation(sc_des1_inst.alarm_data->main_l1_freq_reg, sc_des1_inst.alarm_data->main_l1_phase_reg);
   testSnapshotData.main_l2_l3_phase = PHASE_calculation(sc_des1_inst.alarm_data->main_l2_freq_reg, sc_des1_inst.alarm_data->main_l2_phase_reg);
   testSnapshotData.main_l3_l1_phase = PHASE_calculation(sc_des1_inst.alarm_data->main_l3_freq_reg, sc_des1_inst.alarm_data->main_l3_phase_reg);
   testSnapshotData.main_l1_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_main_l1, sc_des1_inst.sensor_factor->current_main_factor_l1);
   testSnapshotData.main_l2_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_main_l2, sc_des1_inst.sensor_factor->current_main_factor_l2);
   testSnapshotData.main_l3_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_main_l3, sc_des1_inst.sensor_factor->current_main_factor_l3);
   testSnapshotData.main_n_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_main_n, sc_des1_inst.sensor_factor->current_main_factor_n);
   testSnapshotData.f60a_l1_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60a_l1, sc_des1_inst.sensor_factor->current_60A_factor_l1);
   testSnapshotData.f60a_l2_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60a_l2, sc_des1_inst.sensor_factor->current_60A_factor_l2);
   testSnapshotData.f60a_l3_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60a_l3, sc_des1_inst.sensor_factor->current_60A_factor_l3);
   testSnapshotData.f60a_n_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60a_n, sc_des1_inst.sensor_factor->current_60A_factor_n);
   testSnapshotData.f60b_l1_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60b_l1, sc_des1_inst.sensor_factor->current_60B_factor_l1);
   testSnapshotData.f60b_l2_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60b_l2, sc_des1_inst.sensor_factor->current_60B_factor_l2);
   testSnapshotData.f60b_l3_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60b_l3, sc_des1_inst.sensor_factor->current_60B_factor_l3);
   testSnapshotData.f60b_n_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f60b_n, sc_des1_inst.sensor_factor->current_60B_factor_n);
   testSnapshotData.f40a_l1_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40a_l1, sc_des1_inst.sensor_factor->current_40A_factor_l1);
   testSnapshotData.f40a_l2_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40a_l2, sc_des1_inst.sensor_factor->current_40A_factor_l2);
   testSnapshotData.f40a_l3_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40a_l3, sc_des1_inst.sensor_factor->current_40A_factor_l3);
   testSnapshotData.f40a_n_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40a_n, sc_des1_inst.sensor_factor->current_40A_factor_n);
   testSnapshotData.f40b_l1_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40b_l1, sc_des1_inst.sensor_factor->current_40B_factor_l1);
   testSnapshotData.f40b_l2_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40b_l2, sc_des1_inst.sensor_factor->current_40B_factor_l2);
   testSnapshotData.f40b_l3_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40b_l3, sc_des1_inst.sensor_factor->current_40B_factor_l3);
   testSnapshotData.f40b_n_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f40b_n, sc_des1_inst.sensor_factor->current_40B_factor_n);
   testSnapshotData.f20a_l1_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f20a_l1, sc_des1_inst.sensor_factor->current_20A_factor_l1);
   testSnapshotData.f20a_n_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f20a_n, sc_des1_inst.sensor_factor->current_20A_factor_n);
   testSnapshotData.f20b_l2_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f20b_l2, sc_des1_inst.sensor_factor->current_20B_factor_l2);
   testSnapshotData.f20b_n_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_f20b_n, sc_des1_inst.sensor_factor->current_20B_factor_n);
   testSnapshotData.fbyp_l1_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_byp_l1, sc_des1_inst.sensor_factor->current_BYP_factor_l1);
   testSnapshotData.fbyp_l2_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_byp_l2, sc_des1_inst.sensor_factor->current_BYP_factor_l2);
   testSnapshotData.fbyp_l3_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_byp_l3, sc_des1_inst.sensor_factor->current_BYP_factor_l3);
   testSnapshotData.fbyp_n_amp = RMS_calculation(sc_des1_inst.snapshot_data->snapshot_amp_byp_n, sc_des1_inst.sensor_factor->current_BYP_factor_n);
   testSnapshotData.timeHigh = sc_des1_inst.alarm_data->timestamp_seconds_msb & 0x00ffffff;
   testSnapshotData.timeLow = sc_des1_inst.alarm_data->timestamp_seconds_lsb ;
   testSnapshotData.timeNano = sc_des1_inst.alarm_data->timestamp_nanoseconds ;

   // transfer snapshot data
   sourceAddress = (char *)&testSnapshotData.controlReg;
   for ( i = 0; i < snapshotSize; i++ )
   {
      *destAddress++ = *sourceAddress++;
   }



//   // transfer single cycle data set (8x)
//   for ( i = 0; i < SINGLE_CYCLE_BLOCKS; i++ )
//   {
//      sourceAddress = (char *)&testSnapshotData.cycleData[i].main_l1_volt_reg;
//      for ( j = 0; j < cycleSize; j++ )
//      {
//         *destAddress++ = *sourceAddress++;
//      }
//   }

}


//*****************************************************************************
// Function:   getRealTimeData
//
// Marshal the Realtime data for sending to the server.
//
// Input:   output message structure, destination storage address, server connection data
// Output:  int - payload size
//*****************************************************************************
//
static int getRealTimeData( MessageStruct *outputMsg,
                             unsigned char *destAddress,
                             serverConnection *currentServer )
{
   int            payloadSize    = 0;
   unsigned char  *sourceAddress = 0;
   int transferSize;
   int messageLength;
   int currentPingPongBuffer = currentBuffer; // get snapshot of pingpong buffers

   // TODO update message length

   // calculate and transfer payload size
   sourceAddress         = (unsigned char *)&outputMsg->dataLength;
   payloadSize           = currentServer->realtimeBlocksCount;
   outputMsg->dataLength = currentServer->realtimeBlocksCount;

   transferSize = (payloadSize > sizeof(outputBuffer) - (DATA_LENGTH_LOC + sizeof(int))) ? sizeof(outputBuffer) - (DATA_LENGTH_LOC + sizeof(int)) : payloadSize;
// move the length of the data to the output buffer
   Xil_MemCpy(destAddress, sourceAddress, sizeof(int));
   destAddress += sizeof(int);

   // determine which buffer is the buffer to send with data
   if(currentPingPongBuffer == 0) {  // send buffer 2
	   Xil_MemCpy(destAddress, realTimeDataC, transferSize);
	   sourceAddress = (unsigned char *)&realTimeDataC[transferSize/sizeof(u32)];
   } else if(currentPingPongBuffer == 1) {
	   Xil_MemCpy(destAddress, realTimeDataA, transferSize);
	   sourceAddress = (unsigned char *)&realTimeDataA[transferSize/sizeof(u32)];
   } else {
	   Xil_MemCpy(destAddress, realTimeDataB, transferSize);
	   sourceAddress = (unsigned char *)&realTimeDataB[transferSize/sizeof(u32)];
   }

// small requests that fit into a single OutputBuffer
   messageLength   = DATA_LENGTH_LOC + sizeof(int) + transferSize;            // byte count for constant size header information
   long resultsm = sendMessage( outputBuffer, currentServer, messageLength );
   if(resultsm < 0) {  // error condition
	   return resultsm;
   }
   payloadSize -= transferSize;
// large requests that transfer more of the data in the current pingpong buffer
   if(payloadSize > 0){   // more to send
	   if(payloadSize > sizeof(realTimeDataC) - transferSize){  // remainder of the current pingpong buffer
		   transferSize = sizeof(realTimeDataC) - transferSize;
	   } else { // payload size is less than the current pingpong buffer
		   transferSize = payloadSize;
	   }
	   resultsm = sendMessage( sourceAddress, currentServer, transferSize );
	   if(resultsm < 0) {  // error condition
		   return resultsm;
	   }
	   payloadSize -= transferSize;
//WARNING: don't send more than 2 pingpong buffers of data (16MBytes) at one time, controller will hang
// huge requests that transfer data in the next pingpong buffer upto 2 full buffers
	   if(payloadSize > 0){   // more to send, next ping pong buffer
		   if(currentPingPongBuffer == 0)   // send buffer 1
			   sourceAddress = (unsigned char *)&realTimeDataA[0];
		   else if(currentPingPongBuffer == 1)
			   sourceAddress = (unsigned char *)&realTimeDataB[0];
		   else
			   sourceAddress = (unsigned char *)&realTimeDataC[0];

		   if(payloadSize > sizeof(realTimeDataC))  // remainder of the next pingpong buffer
			   transferSize = sizeof(realTimeDataC);  // only send the next pingpong buffer
		   else // payload size is less than the next pingpong buffer
			   transferSize = payloadSize;
		   resultsm = sendMessage( sourceAddress, currentServer, transferSize );
	   }
   }
   return outputMsg->dataLength;
}

#if 0
//*****************************************************************************
// Function:   parseKeyboardInput
//
// This function parses the keyboard string and initiates the requested command
// sequence.  The client information is faked because there is no client so it
// uses the only real client information saved at the startup.
//
// Input:   inputBuffer string, length of inputBuffer string
// Output:
//*****************************************************************************
//
static void parseKeyboardInput( unsigned char *inputBuffer, int inputLength, serverConnection *currentServer )
{
   char  keyboardCmd[3]                   = {0};
   char  msgDataByte                      = 0;
   enum  messageIdentifier newKeyboardCmd = NO_MSG;

   if ( inputLength < 2 )
   {
#ifdef DEBUG
      printf("ParseKeyboardInput error - %s\n", inputBuffer );
#endif
      return;
   }

   // parse command/response, create message string
   keyboardCmd[0] = toupper( inputBuffer[0] );
   keyboardCmd[1] = toupper( inputBuffer[1] );

   newKeyboardCmd = s_messageMap[keyboardCmd];

   switch ( newKeyboardCmd )
   {
      case ACK_MSG:
      case ALARM_RESPONSE_MSG:
      case CONFIG_UPDATE_MSG:
      case HB_MSG:
      case NAK_MSG:
      case NO_DATA_MSG:
      case REALTIME_DATA_MSG:
      case SNAPSHOT_DATA_MSG:
      case ILLEGAL_MSG:
         ;     // nothing to do here for these commands
         break;

      default:
#ifdef DEBUG
         printf( "ParseKeyboardInput error - illegal command - exiting\n" );
#endif
         timeToQuit = true;
         return;
         break;
   }  // end of main switch

   responseParser( newKeyboardCmd, msgDataByte, currentServer );
}

#endif

//*****************************************************************************
// Function:   messageParser
//
// This function parses message blocks to determine content and decide what needs
// to be done with/for this message from the server.
//
// Input:
// Output:
//*****************************************************************************
//
int messageParser( unsigned char *msg, serverConnection *currentServer )
{
   int            i              = 0;
   unsigned char  *destAddress   = 0;
   double         *tmpPtr        = 0;
   MessageStruct  inputMsg;

   memset( (void *)&inputMsg, 0, sizeof( MessageStruct ) );

   // extract message components
   inputMsg.deviceID    = *((long long *)msg);        msg += sizeof(long long);
   inputMsg.messageID   = *((int *)msg);              msg += sizeof(int);
   inputMsg.cmd[0]      = *((char *)msg);             msg += sizeof(char);
   inputMsg.cmd[1]      = *((char *)msg);             msg += sizeof(char);
   inputMsg.cmd[2]      = *((char *)msg);             msg += sizeof(char);
   inputMsg.cmd[3]      = *((char *)msg);             msg += sizeof(char);
   inputMsg.dataLength  = *((int *)msg);              msg += sizeof(int);

   // KLM: debug
#ifdef DEBUG
      printf("DBG messageParser: devID %lld, mesID %d, length %d\n ",
             inputMsg.deviceID, inputMsg.messageID, inputMsg.dataLength );
#endif
   // KLM: debug

   // parse command/response, create message string
   inputMsg.cmd[0]  = toupper( inputMsg.cmd[0] );
   inputMsg.cmd[1]  = toupper( inputMsg.cmd[1] );
   inputMsg.cmd[2]  = 0;                           // null terminate string

   // verify message has been sent to the correct device
   if ( inputMsg.deviceID != currentServer->deviceID )
   {
      printf( "Device IDs do not match, server %lld, client %lld\n",
              inputMsg.deviceID, currentServer->deviceID );
      return -1;
      // KLM:todo send msg to data logger
   }

   // verify message ID has the correct value - checking for missing messages
   // or failed message processing.
   currentServer->messageID++;                           // update message index count
   if ( currentServer->messageID != inputMsg.messageID ) // verify message count
   {
      printf( "Message ID does not match expected: server %d, client %d\n",
              currentServer->messageID, inputMsg.messageID );

      currentServer->messageID = inputMsg.messageID;     // reset message IDs but ignore this message
      responseParser( NAK_MSG, 0, currentServer );       // respond with a NAK
      return 0;
      // KLM:todo send msg to data logger
   }

   switch ( s_messageMap[inputMsg.cmd] )
   {
      case ACK_MSG:                  // acknowledge
         if ( currentServer->expectedResponse == ACK_MSG )
         {
            currentServer->expectedResponse = NO_MSG;             // consume expected
         }
         else
         {
            printf( "ACK_ERR, unexpected\n" );
            // KLM:todo send msg to data logger
         }
         break;

      case FEED_SHED_PRIORITY_MSG:     // received new feed sheding priorities
#ifdef DEBUG
         printf("FS-processing\n");    // KLM: debug
#endif

         currentServer->expectedResponse = NO_MSG;    // no response from server is expected

         // setup data transfer
         destAddress    = (unsigned char *)&testFeedShedPriority.main_60A_priority;

         for ( i = 0; i < inputMsg.dataLength; i++ )
         {
            *destAddress++ = *msg++;
         }

         // display new priorities
#ifdef DEBUG
         printf( "Feed Shed Priorities\n" );
         printf( "%-5d, %-5d, %-5d, %-5d, %-5d, %-5d\n",
                 testFeedShedPriority.main_60A_priority,
                 testFeedShedPriority.main_60B_priority,
                 testFeedShedPriority.main_40A_priority,
                 testFeedShedPriority.main_60B_priority,
                 testFeedShedPriority.main_20A_priority,
                 testFeedShedPriority.main_20B_priority
               );
#endif

         // send ack to server
         responseParser( ACK_MSG, 0, currentServer );
         break;

      case HB_MSG:                   // heartbeat
         currentServer->expectedResponse = NO_MSG;    // consume if it was expected

         // send response to HB message
         responseParser( ACK_MSG, 0, currentServer );
         break;

      case GET_ALARMS_MSG:            // get alarms
         currentServer->expectedResponse = NO_MSG;    // no response from server is expected

         // send alarm response to server
         responseParser( ALARM_RESPONSE_MSG, 0, currentServer );
         break;

      case GET_CONFIG_MSG:            // get configuration
         currentServer->expectedResponse = NO_MSG;    // no response from server is expected

         // send configuration to server
         responseParser( CONFIG_UPDATE_MSG, 0, currentServer );
         break;

      case GET_REALTIME_DATA_MSG:      // get real-time data
         currentServer->expectedResponse = NO_MSG;    // no response from server is expected

         // transfer data byte
         Xil_MemCpy(&currentServer->realtimeBlocksCount, msg, sizeof(int));
//         currentServer->realtimeBlocksCount = *msg;

         // send real-time data to server
         responseParser( REALTIME_DATA_MSG, 0, currentServer );
         break;

      case GET_SNAPSHOT_DATA_MSG:      // get snapshot data
         currentServer->expectedResponse = NO_MSG;    // no response from server is expected

         // send configuration to server
         responseParser( SNAPSHOT_DATA_MSG, 0, currentServer );
         break;

      case GET_TEMPERATURE_MSG: // get temperature data
          currentServer->expectedResponse = NO_MSG;    // no response from server is expected
          //send temperature data
          responseParser( RESPONSE_TEMPERATURE_MSG, 0, currentServer );
    	  break;
      case NAK_MSG:                  // negative acknowledge
         if ( currentServer->expectedResponse == ACK_MSG )
         {
            currentServer->expectedResponse = NO_MSG;             // consume expected
         }
         printf( "NAK_ERR\n" );
         // KLM:todo send msg to data logger
         break;

      case SET_ALARM_LIMITS_MSG:       // set alarm limits
         currentServer->expectedResponse = NO_MSG;

         // transfer data block
         destAddress   = (unsigned char *)&updatedAlarmLimits;

         for ( i = 0; i < inputMsg.dataLength; i++ )
         {
            *destAddress++ = *msg++;
         }
         sc_des1_inst.alarm_data->main_l1_volt_max_reg = (uint32_t)updatedAlarmLimits.main_l1_volt_max;
         sc_des1_inst.alarm_data->main_l1_volt_min_reg = (uint32_t)updatedAlarmLimits.main_l1_volt_min;
         sc_des1_inst.alarm_data->main_l2_volt_max_reg = (uint32_t)updatedAlarmLimits.main_l2_volt_max;
         sc_des1_inst.alarm_data->main_l2_volt_min_reg = (uint32_t)updatedAlarmLimits.main_l2_volt_min;
         sc_des1_inst.alarm_data->main_l3_volt_max_reg = (uint32_t)updatedAlarmLimits.main_l3_volt_max;
         sc_des1_inst.alarm_data->main_l3_volt_min_reg = (uint32_t)updatedAlarmLimits.main_l3_volt_min;
         sc_des1_inst.alarm_data->main_freq_max_reg = (uint32_t)updatedAlarmLimits.main_freq_max;
         sc_des1_inst.alarm_data->main_freq_min_reg = (uint32_t)updatedAlarmLimits.main_freq_min;
         sc_des1_inst.alarm_data->main_phase_max_reg = (uint32_t)updatedAlarmLimits.main_phase_max;
         sc_des1_inst.alarm_data->main_phase_min_reg = (uint32_t)updatedAlarmLimits.main_phase_min;
         sc_des1_inst.alarm_data->main_l1_amp_max_reg = (uint32_t)updatedAlarmLimits.main_l1_amp_max;
         sc_des1_inst.alarm_data->main_l2_amp_max_reg = (uint32_t)updatedAlarmLimits.main_l2_amp_max;
		 sc_des1_inst.alarm_data->main_l3_amp_max_reg = (uint32_t)updatedAlarmLimits.main_l3_amp_max;
		 sc_des1_inst.alarm_data->main_n_amp_max_reg = (uint32_t)updatedAlarmLimits.main_n_amp_max;
         sc_des1_inst.alarm_data->f60a_l1_amp_max_reg = (uint32_t)updatedAlarmLimits.f60a_l1_amp_max;
         sc_des1_inst.alarm_data->f60a_l2_amp_max_reg = (uint32_t)updatedAlarmLimits.f60a_l2_amp_max;
		 sc_des1_inst.alarm_data->f60a_l3_amp_max_reg = (uint32_t)updatedAlarmLimits.f60a_l3_amp_max;
 		 sc_des1_inst.alarm_data->f60a_n_amp_max_reg = (uint32_t)updatedAlarmLimits.f60a_n_amp_max;
         sc_des1_inst.alarm_data->f60b_l1_amp_max_reg = (uint32_t)updatedAlarmLimits.f60b_l1_amp_max;
         sc_des1_inst.alarm_data->f60b_l2_amp_max_reg = (uint32_t)updatedAlarmLimits.f60b_l2_amp_max;
		 sc_des1_inst.alarm_data->f60b_l3_amp_max_reg = (uint32_t)updatedAlarmLimits.f60b_l3_amp_max;
		 sc_des1_inst.alarm_data->f60b_n_amp_max_reg = (uint32_t)updatedAlarmLimits.f60b_n_amp_max;
         sc_des1_inst.alarm_data->f40a_l1_amp_max_reg = (uint32_t)updatedAlarmLimits.f40a_l1_amp_max;
         sc_des1_inst.alarm_data->f40a_l2_amp_max_reg = (uint32_t)updatedAlarmLimits.f40a_l2_amp_max;
		 sc_des1_inst.alarm_data->f40a_l3_amp_max_reg = (uint32_t)updatedAlarmLimits.f40a_l3_amp_max;
		 sc_des1_inst.alarm_data->f40a_n_amp_max_reg = (uint32_t)updatedAlarmLimits.f40a_n_amp_max;
         sc_des1_inst.alarm_data->f40b_l1_amp_max_reg = (uint32_t)updatedAlarmLimits.f40b_l1_amp_max;
         sc_des1_inst.alarm_data->f40b_l2_amp_max_reg = (uint32_t)updatedAlarmLimits.f40b_l2_amp_max;
		 sc_des1_inst.alarm_data->f40b_l3_amp_max_reg = (uint32_t)updatedAlarmLimits.f40b_l3_amp_max;
		 sc_des1_inst.alarm_data->f40b_n_amp_max_reg = (uint32_t)updatedAlarmLimits.f40b_n_amp_max;
         sc_des1_inst.alarm_data->f20a_l1_amp_max_reg = (uint32_t)updatedAlarmLimits.f20a_l1_amp_max;
         sc_des1_inst.alarm_data->f20a_n_amp_max_reg = (uint32_t)updatedAlarmLimits.f20a_n_amp_max;
		 sc_des1_inst.alarm_data->f20b_l2_amp_max_reg = (uint32_t)updatedAlarmLimits.f20b_l2_amp_max;
		 sc_des1_inst.alarm_data->f20b_n_amp_max_reg = (uint32_t)updatedAlarmLimits.f20b_n_amp_max;
         sc_des1_inst.alarm_data->fbyp_l1_amp_max_reg = (uint32_t)updatedAlarmLimits.fbyp_l1_amp_max;
         sc_des1_inst.alarm_data->fbyp_l2_amp_max_reg = (uint32_t)updatedAlarmLimits.fbyp_l2_amp_max;
		 sc_des1_inst.alarm_data->fbyp_l3_amp_max_reg = (uint32_t)updatedAlarmLimits.fbyp_l3_amp_max;
		 sc_des1_inst.alarm_data->fbyp_n_amp_max_reg = (uint32_t)updatedAlarmLimits.fbyp_n_amp_max;


         // display new limits
         set_alarm_limits((ALARM_LIMIT_T*)&sc_des1_inst.alarm_data->main_l1_volt_max_reg);
//#ifdef DEBUG
         printf( "New Alarm Limits\n" );
         printf("Volt L1 %d %d\n",sc_des1_inst.alarm_data->main_l1_volt_max_reg,sc_des1_inst.alarm_data->main_l1_volt_min_reg);
         printf("Volt L2 %d %d\n",sc_des1_inst.alarm_data->main_l2_volt_max_reg,sc_des1_inst.alarm_data->main_l2_volt_min_reg);
         printf("Volt L3 %d %d\n",sc_des1_inst.alarm_data->main_l3_volt_max_reg,sc_des1_inst.alarm_data->main_l3_volt_min_reg);
         printf("Freq    %d %d\n",sc_des1_inst.alarm_data->main_freq_max_reg,sc_des1_inst.alarm_data->main_freq_min_reg);
         printf("Phase   %d %d\n",sc_des1_inst.alarm_data->main_phase_max_reg,sc_des1_inst.alarm_data->main_phase_min_reg);
		 printf("MAIN    %d %d %d %d\n", sc_des1_inst.alarm_data->main_l1_amp_max_reg, sc_des1_inst.alarm_data->main_l2_amp_max_reg,sc_des1_inst.alarm_data->main_l3_amp_max_reg,sc_des1_inst.alarm_data->main_n_amp_max_reg);
		 printf("60A     %d %d %d %d\n", sc_des1_inst.alarm_data->f60a_l1_amp_max_reg, sc_des1_inst.alarm_data->f60a_l2_amp_max_reg,sc_des1_inst.alarm_data->f60a_l3_amp_max_reg,sc_des1_inst.alarm_data->f60a_n_amp_max_reg);
		 printf("60B     %d %d %d %d\n", sc_des1_inst.alarm_data->f60b_l1_amp_max_reg, sc_des1_inst.alarm_data->f60b_l2_amp_max_reg,sc_des1_inst.alarm_data->f60b_l3_amp_max_reg,sc_des1_inst.alarm_data->f60b_n_amp_max_reg);
		 printf("40A     %d %d %d %d\n", sc_des1_inst.alarm_data->f40a_l1_amp_max_reg, sc_des1_inst.alarm_data->f40a_l2_amp_max_reg,sc_des1_inst.alarm_data->f40a_l3_amp_max_reg,sc_des1_inst.alarm_data->f40a_n_amp_max_reg);
		 printf("40B     %d %d %d %d\n", sc_des1_inst.alarm_data->f40b_l1_amp_max_reg, sc_des1_inst.alarm_data->f40b_l2_amp_max_reg,sc_des1_inst.alarm_data->f40b_l3_amp_max_reg,sc_des1_inst.alarm_data->f40b_n_amp_max_reg);
		 printf("20A     %d %d\n", sc_des1_inst.alarm_data->f20a_l1_amp_max_reg, sc_des1_inst.alarm_data->f20a_n_amp_max_reg);
		 printf("20B     %d %d\n", sc_des1_inst.alarm_data->f20b_l2_amp_max_reg, sc_des1_inst.alarm_data->f20b_n_amp_max_reg);
		 printf("BYP     %d %d %d %d\n", sc_des1_inst.alarm_data->fbyp_l1_amp_max_reg, sc_des1_inst.alarm_data->fbyp_l2_amp_max_reg,sc_des1_inst.alarm_data->fbyp_l3_amp_max_reg,sc_des1_inst.alarm_data->fbyp_n_amp_max_reg);
//#endif
         // send ack to server
         responseParser( ACK_MSG, 0, currentServer );
         break;

      case SET_CONFIG_MSG:            // set voltage and current factors (config)
         currentServer->expectedResponse = NO_MSG;

         // transfer data block
         destAddress   = (unsigned char *)&sc_des1_inst.sensor_factor->volt_factor_l1;

         for ( i = 0; i < inputMsg.dataLength; i++ )
         {
            *destAddress++ = *msg++;
         }

         // display new factors
// update required for correction factors for each voltage and each current sensor individually //
// set at beginning of time in void dummy_set_sensor_factor();
//#ifdef DEBUG
         printf( "New Config Limits\n" );
         printf( "v1: %-15lf, v2: %-15lf, v3: %-15lf\n", sc_des1_inst.sensor_factor->volt_factor_l1, sc_des1_inst.sensor_factor->volt_factor_l2,sc_des1_inst.sensor_factor->volt_factor_l3 );
         printf( "main_l1: %-15lf, main_l2: %-15lf, main_l3: %-15lf main_n: %-15lf\n", sc_des1_inst.sensor_factor->current_main_factor_l1, sc_des1_inst.sensor_factor->current_main_factor_l2,
        		 sc_des1_inst.sensor_factor->current_main_factor_l3, sc_des1_inst.sensor_factor->current_main_factor_n);
         printf( "BYP_l1: %-15lf, BYP_l2: %-15lf, BYP_l3: %-15lf BYP_n: %-15lf\n", sc_des1_inst.sensor_factor->current_BYP_factor_l1, sc_des1_inst.sensor_factor->current_BYP_factor_l2,
        		 sc_des1_inst.sensor_factor->current_BYP_factor_l3, sc_des1_inst.sensor_factor->current_BYP_factor_n);
         printf( "60A_l1: %-15lf, 60A_l2: %-15lf, 60A_l3: %-15lf 60A_n: %-15lf\n", sc_des1_inst.sensor_factor->current_60A_factor_l1, sc_des1_inst.sensor_factor->current_60A_factor_l2,
        		 sc_des1_inst.sensor_factor->current_60A_factor_l3, sc_des1_inst.sensor_factor->current_60A_factor_n);
         printf( "60B_l1: %-15lf, 60B_l2: %-15lf, 60B_l3: %-15lf 60B_n: %-15lf\n", sc_des1_inst.sensor_factor->current_60B_factor_l1, sc_des1_inst.sensor_factor->current_60B_factor_l2,
        		 sc_des1_inst.sensor_factor->current_60B_factor_l3, sc_des1_inst.sensor_factor->current_60B_factor_n);
         printf( "40A_l1: %-15lf, 40A_l2: %-15lf, 40A_l3: %-15lf 40A_n: %-15lf\n", sc_des1_inst.sensor_factor->current_40A_factor_l1, sc_des1_inst.sensor_factor->current_40A_factor_l2,
        		 sc_des1_inst.sensor_factor->current_40A_factor_l3, sc_des1_inst.sensor_factor->current_40A_factor_n);
         printf( "40B_l1: %-15lf, 40B_l2: %-15lf, 40B_l3: %-15lf 40B_n: %-15lf\n", sc_des1_inst.sensor_factor->current_40B_factor_l1, sc_des1_inst.sensor_factor->current_40B_factor_l2,
        		 sc_des1_inst.sensor_factor->current_40B_factor_l3, sc_des1_inst.sensor_factor->current_40B_factor_n);
         printf( "20A_l1: %-15lf,  20A_n: %-15lf\n", sc_des1_inst.sensor_factor->current_20A_factor_l1, sc_des1_inst.sensor_factor->current_20A_factor_n);
         printf( "20B_l2: %-15lf,  20B_n: %-15lf\n", sc_des1_inst.sensor_factor->current_20B_factor_l2, sc_des1_inst.sensor_factor->current_20B_factor_n);
 //#endif
         // send ack to server
         responseParser( ACK_MSG, 0, currentServer );
         break;

      case SET_ENABLE_MSG:            // set enable
         currentServer->expectedResponse = NO_MSG;

         CONTROL_REG_T local_control_reg;
     	 local_control_reg.reg = sc_des1_inst.alarm_data->control_reg.reg;
     	 local_control_reg.bits.streamCollectEnable = (uint32_t)*msg;  // stop/start collecting streaming data
//     	 local_control_reg.bits.streamIntEnable = (uint32_t)*msg;
     	 set_control_reg(&local_control_reg);

         // transfer data byte
         currentServer->enableOperation = *msg;
// Currently set at beginning of time  dummy for now//
#ifdef DEBUG
         printf( "Set Enable to %d\n", currentServer->enableOperation );
#endif

         // send ack to server
         responseParser( ACK_MSG, 0, currentServer );
         break;

      case SET_INPUT_FREQUENCY_MSG:    // set input frequency
         currentServer->inputFrequency = NO_MSG;

         // transfer data byte
         currentServer->inputFrequency = *msg;
         set_system_frequency((int)*msg);

#ifdef DEBUG
         printf( "Set Input Frequency to %d\n", currentServer->inputFrequency );
#endif
         // send ack to server
         responseParser( ACK_MSG, 0, currentServer );
         break;

      case SET_LEDS_MSG:              // set LED
         currentServer->expectedResponse = NO_MSG;

         // transfer data byte
         currentServer->enableLeds = *msg;
// Need to fix I2C for restart before this will work //
#ifdef DEBUG
         printf( "Set LEDs to %d\n", currentServer->enableLeds );
#endif

         // send ack to server
         responseParser( ACK_MSG, 0, currentServer );
         break;

      case SET_MONITORING_MSG:        // set monitoring
         currentServer->expectedResponse = NO_MSG;

         // transfer data byte
         currentServer->enableMonitoring = *msg;
// Currently set at beginning of time  dummy for now//
#ifdef DEBUG
         printf( "Set Monitoring to %d\n", currentServer->enableMonitoring );
#endif

         // send ack to server
         responseParser( ACK_MSG, 0, currentServer );
         break;

      case SET_FEED_CONTROL_MSG:       // set feed control (shed power)
         currentServer->expectedResponse = NO_MSG;

         // transfer data byte
         currentServer->feedControl = *msg;
         CONTACTOR_DISENGAGE_T contactor_setting;
         contactor_setting.reg = (u32_t)*msg;
         set_contactor(contactor_setting);
#ifdef DEBUG
         printf( "Set Feed Control to %d\n", currentServer->feedControl );
#endif
         // send ack to server
         responseParser( ACK_MSG, 0, currentServer );
         break;

      case SET_STREAMING_MSG:         // set streaming
         currentServer->expectedResponse = NO_MSG;

         // transfer data byte
         currentServer->enableStreaming = *msg;
// Likely never be called, dummy for now//
#ifdef DEBUG
         printf( "Set Streaming to %d\n", currentServer->enableStreaming );
#endif
         // send ack to server
         responseParser( ACK_MSG, 0, currentServer );
         break;

      case SET_TIME_MSG:              // set time
         currentServer->expectedResponse = NO_MSG;

         // transfer data block
         destAddress   = (unsigned char *)&testTimeValue.timeHigh;

         for ( i = 0; i < inputMsg.dataLength; i++ )
         {
            *destAddress++ = *msg++;
         }
//         u32 seconds = (((u64) testTimeValue.timeHigh << 32) && 0x00000000FFFFFFFF) | ((u64) testTimeValue.timeLow && 0x00000000FFFFFFFF);
//         u32 nanoseconds = testTimeValue.timeNano;
         set_timestamp_direct(testTimeValue.timeHigh, testTimeValue.timeLow, testTimeValue.timeNano);

#ifdef DEBUG
         printf( "New Time value\n" );
         printf( "%ld\n", testTimeValue.timeHigh );
         printf( "%ld\n", testTimeValue.timeLow );
         printf( "%ld\n", testTimeValue.timeNano );
#endif

         // send ack to server
         responseParser( ACK_MSG, 0, currentServer );
         break;

      case GET_NODATA_MSG:    // KLM:debug - this is only necessary for testing
         responseParser( NO_DATA_MSG, 0, currentServer );
         break;

      default:
#ifdef DEBUG
         printf( "UNKNOWN MSG, msg %s, device %lld, msgID %d\n",
                 inputMsg.cmd,
                 inputMsg.deviceID,
                 inputMsg.messageID );
         // KLM:todo send msg to data logger
#endif
         break;
   }
   return 0;
   // perform necessary action
}


//*****************************************************************************
// Function:   SendMessage
//
// This function sends the message to the IPDU device
// If the queue is empty, the message is sent,  If the queue is not empty, the message is enqueued
//
// Input:   pointer to output data, currentClient information and message length
// Output:  byte count of send data
//*****************************************************************************
//
static int sendMessage( void *outputBuffer, serverConnection *currentServer, int byteCount ){
	// check if queue is empty, if so, put the message on the queue and send it
	if(enqueueIndex == dequeueIndex){
		sendMsgQueue[enqueueIndex].outputBuffer = outputBuffer;
		sendMsgQueue[enqueueIndex].currentServer = currentServer;
		sendMsgQueue[enqueueIndex].byteCount = byteCount;
		int bytesSent;
		bytesSent = outputMessage( outputBuffer, currentServer, byteCount );
		if(bytesSent == byteCount || bytesSent < 0){ // Successful or error, not a partial packet
			enqueueIndex = (enqueueIndex == SEND_MSG_QUEUE_LEN - 1) ? 0 : enqueueIndex + 1;
			dequeueIndex = (dequeueIndex == SEND_MSG_QUEUE_LEN - 1) ? 0 : dequeueIndex + 1;
		} else { // partial packet: leave the rest on the queue
			sendMsgQueue[enqueueIndex].outputBuffer += bytesSent;
			sendMsgQueue[enqueueIndex].byteCount -= bytesSent;
			enqueueIndex = (enqueueIndex == SEND_MSG_QUEUE_LEN - 1) ? 0 : enqueueIndex + 1;
		}
	} else { // something already on the queue, just add to the end
		if(((enqueueIndex == SEND_MSG_QUEUE_LEN - 1) ? 0 : enqueueIndex + 1) == dequeueIndex) {
			printf("queue full discarding message\n");
			return -1;
		} else {
			sendMsgQueue[enqueueIndex].outputBuffer = outputBuffer;
			sendMsgQueue[enqueueIndex].currentServer = currentServer;
			sendMsgQueue[enqueueIndex].byteCount = byteCount;
			enqueueIndex = (enqueueIndex == SEND_MSG_QUEUE_LEN - 1) ? 0 : enqueueIndex + 1;
		}
	}
	return byteCount;  //TODO check return value
}

//*****************************************************************************
// Function:   nextSendMsg
//
// This function is called from the lwIP send callback function when a message has been successfully sent
// If the queue is empty, no further action is needed
//If the queue is not empty, the next message on the queue is sent
//
// Input:  none
// Output:  error indication
//*****************************************************************************
//

err_t nextSendMsg(){
	int bytesSent;

	if(enqueueIndex == dequeueIndex)  // nothing to do
		return ERR_OK;
	else {
		bytesSent = outputMessage(sendMsgQueue[dequeueIndex].outputBuffer,
				                  sendMsgQueue[dequeueIndex].currentServer,
								  sendMsgQueue[dequeueIndex].byteCount);
		if(bytesSent == sendMsgQueue[dequeueIndex].byteCount || bytesSent < 0){ // Successful or error, not a partial packet
			dequeueIndex = (dequeueIndex == SEND_MSG_QUEUE_LEN - 1) ? 0 : dequeueIndex + 1;
		} else { // partial packet: leave the rest on the queue
			sendMsgQueue[dequeueIndex].outputBuffer += bytesSent;
			sendMsgQueue[dequeueIndex].byteCount -= bytesSent;
		}
	}
	return (bytesSent < 0) ? bytesSent : ERR_OK;
}


//*****************************************************************************
// Function:   outputMessage
//
// This function is called from either sendMessage or nextSendMsg
// and places the data in outputBuffer onto the network.  The data can be upto byteCount or tcpSendbuf() size
//
// Input:  output buffer is a pointer to a bytestream of data
// 			currentServer is TCP connection
//		   bytecount is the number of bytes to send
// Output:  error indication or number of bytes
//*****************************************************************************
//
static int outputMessage( void *outputBuffer, serverConnection *currentServer, int byteCount )
{
	err_t   result;
	int sendAmount;

#ifdef DEBUG
	printf( "Sending Msg\n" );
	printf("sendMessage byteCount %d tcp_sndbuf %d\n", byteCount, tcp_sndbuf(pcb));
#endif
	sendAmount = (byteCount > tcp_sndbuf(pcb)) ? tcp_sndbuf(pcb) : byteCount;
		//   ssize_t result = send( currentServer->fd, outputBuffer, byteCount, 0 );
		result = tcp_write(pcb,outputBuffer, sendAmount, 0);
		if ( result < 0 )
		{
			printf("tcp_write failed, errorno %d, desc %s\n", errno, strerror(errno));

			if ( errno == EMSGSIZE )
			{
				printf("emsgsize error\n" );
			}
			return result;
		}
		result = tcp_output(pcb);
		if ( result < 0 )
		{
			printf("tcp_output failed, errorno %d, desc %s\n", errno, strerror(errno));

			if ( errno == EMSGSIZE )
			{
				printf("emsgsize error\n" );
			}
		}
	return (result < 0) ? result : sendAmount;
}

//*****************************************************************************
// Function:   responseParser
//
// This function configures the command for the selected IPDU simulator
//
// Input:   enumerated message, single byte data (if needed), currentClient information
// Output:  0 completed, -1 failed
//*****************************************************************************
//
int responseParser( enum messageIdentifier newOutputMsg,
                           char msgDataByte,   //overload this function to not check if the message is a response or an autonomous alarm
                           serverConnection *currentServer )
{
   int            i           = 0;
   int            payloadSize = 0;
   MessageStruct  outputMsg;

   memset( (void *)&outputMsg, 0, sizeof( MessageStruct ) );
   memset( (void *)&outputBuffer, 0xFF, sizeof( outputBuffer) );     // KLM:debug purposes ??

   // extract message components
   if(msgDataByte == 0) { // actual response as opposed to autonomous message
	   currentServer->messageID++;
   	   outputMsg.messageID  = currentServer->messageID;
   } else {
   	   outputMsg.messageID  = 0;
   }

   	   outputMsg.deviceID   = currentServer->deviceID;

   // load output buffer with complete message
   unsigned char *sourceAddress  = (unsigned char *)&outputMsg.deviceID;
   unsigned char *destAddress    = outputBuffer;
   int  messageLength   = DATA_LENGTH_LOC;            // byte count for constant size header information

#ifdef DEBUG
   printf("response parser %d\n",newOutputMsg);
#endif
   switch ( newOutputMsg )
   {
      case ACK_MSG:                  // acknowledge
         outputMsg.cmd[0]  = 'A';
         outputMsg.cmd[1]  = 'K';
         outputMsg.dataLength  = 0;
         outputMsg.messageData = NULL;
         currentServer->expectedResponse = NO_MSG;

         messageLength += sizeof(int);           // add in size of data length field

         // transfer deviceID, messageID, message and data length field
         for ( i = 0; i < messageLength; i++ )    // KLM:todo byte count is +1
         {
            *destAddress++ = *sourceAddress++;
         }
         break;

      case HB_MSG:                   // heartbeat
#ifdef DEBUG
         printf("DBG-processing HB\n" );
#endif

         outputMsg.cmd[0]  = 'H';
         outputMsg.cmd[1]  = 'B';
         outputMsg.dataLength  = 0;
         outputMsg.messageData = NULL;
         currentServer->expectedResponse = ACK_MSG;

         messageLength += sizeof(int);           // add in size of data length field

         // transfer deviceID, messageID, message and data length field
         for ( i = 0; i < messageLength; i++ )    // KLM:todo byte count is +1
         {
            *destAddress++ = *sourceAddress++;
         }
         break;

      case NO_DATA_MSG:               // no data
         outputMsg.cmd[0]  = 'N';
         outputMsg.cmd[1]  = 'D';
         outputMsg.dataLength  = 0;
         outputMsg.messageData = NULL;
         currentServer->expectedResponse = NO_MSG;

         messageLength += sizeof(int);           // add in size of data length field

         // transfer deviceID, messageID, message and data length field
         for ( i = 0; i < messageLength; i++ )    // KLM:todo byte count is +1
         {
            *destAddress++ = *sourceAddress++;
         }
         break;

      case RESPONSE_TEMPERATURE_MSG:               // double for temperature
         outputMsg.cmd[0]  = 'R';
         outputMsg.cmd[1]  = 'T';
         outputMsg.dataLength  = sizeof(double);
         currentServer->expectedResponse = NO_MSG;

         messageLength += sizeof(int);           // add in size of data length field

         // transfer deviceID, messageID, message and data length field
         for ( i = 0; i < messageLength; i++ )    // KLM:todo byte count is +1
         {
            *destAddress++ = *sourceAddress++;
         }

         // get payload data block
         sourceAddress = (unsigned char*)&sc_des1_inst.temperature;
         for ( i = 0; i < messageLength; i++ )    // KLM:todo byte count is +1
         {
            *destAddress++ = *sourceAddress++;
         }
         messageLength += outputMsg.dataLength;     // add in payload size
         break;

      case NAK_MSG:                  // negative acknowledge
         outputMsg.cmd[0]  = 'N';
         outputMsg.cmd[1]  = 'K';
         outputMsg.dataLength  = 0;
         outputMsg.messageData = NULL;
         currentServer->expectedResponse = NO_MSG;

         messageLength += sizeof(int);           // add in size of data length field

         // transfer deviceID, messageID, message and data length field
         for ( i = 0; i < messageLength; i++ )    // KLM:todo byte count is +1
         {
            *destAddress++ = *sourceAddress++;
         }
         break;

      case ALARM_RESPONSE_MSG:        // alarm update
         outputMsg.cmd[0]  = 'R';
         outputMsg.cmd[1]  = 'A';
         currentServer->expectedResponse = NO_MSG;

         messageLength += sizeof(int);     // add in size of data length field

         outputMsg.dataLength  =   (char*)&testAlarmData.endOfStruct
                                 - (char*)&testAlarmData.alarmType;

#ifdef DEBUG
         printf("DBG - RA length %d, sizeof %ld\n", outputMsg.dataLength, sizeof(unsigned long) );      // KLM: debug
#endif

         // transfer deviceID, messageID, message and data length field
         for ( i = 0; i < messageLength; i++ )    // KLM:todo byte count is +1
         {
            *destAddress++ = *sourceAddress++;
         }

         // get payload data block.  This structure is filled in by GenerateAlarmMessage() and placed into testAlarmData
         GenerateAlarmMessage();
         sourceAddress = (unsigned char*)&testAlarmData.alarmType;
         for ( i = 0; i < outputMsg.dataLength; i++ )
         {
            *destAddress++ = *sourceAddress++;
         }
         messageLength += outputMsg.dataLength;     // add in payload size
         break;

      case CONFIG_UPDATE_MSG:         // configuration update
         outputMsg.cmd[0]  = 'R';
         outputMsg.cmd[1]  = 'C';
         currentServer->expectedResponse = NO_MSG;

         // transfer deviceID, messageID and message
         for ( i = 0; i < messageLength; i++ )    // KLM:todo byte count is +1
         {
            *destAddress++ = *sourceAddress++;
         }

         // transfer payload and payload length to output
         getConfigData( &outputMsg, destAddress, currentServer );

         messageLength += sizeof(int);           // add in size of data length field
         messageLength += outputMsg.dataLength;  // add in payload size
         break;

      case SNAPSHOT_DATA_MSG:         // snapshot data
         outputMsg.cmd[0]  = 'R';
         outputMsg.cmd[1]  = 'S';
         currentServer->expectedResponse = NO_MSG;

         // transfer deviceID, messageID and message
         for ( i = 0; i < messageLength; i++ )    // KLM:todo byte count is +1
         {
            *destAddress++ = *sourceAddress++;
         }

         // transfer payload and payload length to output
         getSnapshotData( &outputMsg, destAddress, currentServer );      // transfer to output

         messageLength += sizeof(int);           // add in size of data length field
         messageLength += outputMsg.dataLength;  // add in payload size
         break;

      case REALTIME_DATA_MSG:         // real-time data
         outputMsg.cmd[0]  = 'R';
         outputMsg.cmd[1]  = 'R';
         currentServer->expectedResponse = NO_MSG;

         // transfer deviceID, messageID and message
         for ( i = 0; i < messageLength; i++ )    // KLM:todo byte count is +1
         {
            *destAddress++ = *sourceAddress++;
         }

         // transfer payload and payload length to output
// send real-time data from within getRealTimeData
         payloadSize = getRealTimeData( &outputMsg, destAddress, currentServer );      // transfer to output

// the following is kept for debugging purposes, data is sent in getRealTimeData
         messageLength += sizeof(int);          // add in size of data length field
         messageLength += payloadSize;          // add in payload size
         break;

      default:
#ifdef DEBUG
         printf( "UNKNOWN CMD, msg %d, device %lld, msgID %d\n",
                 newOutputMsg,
                 outputMsg.deviceID,
                 outputMsg.messageID );
         // KLM:todo send msg to data logger
#endif
         return -1;
         break;
   }
#ifdef DEBUG
   printf("DBG: outputbuffer contents\n");    // KLM: debug
   for ( int k=0; k < 20; k++)
   {
      printf("out[%d] %x\n", k, outputBuffer[k] );
   }
   printf( "DBG msg struct devID %lld, mesID %d, len %d\n",
           outputMsg.deviceID, outputMsg.messageID, outputMsg.dataLength );


   printf( "Sending data byte count %d\n", messageLength );
#endif
   if(!(outputMsg.cmd[0] == 'R' && outputMsg.cmd[1] == 'R')) {  // sendMessage is handled differently for Realtime data
	   long resultsm = sendMessage( outputBuffer, currentServer, messageLength );
   }
#ifdef DEBUG
   printf("SendMessage result %ld\n", resultsm );
#endif
   return 0;
}


//*****************************************************************************
// Function:   main
//
// This is where it all begins.
//
// This also contains the main control loop. (For now) it is setup to be a simple
// echo loop for building/testing the command parser.
//
// The communication interface is setup to work with using the loopBack connection
// at 127.0.0.1 with port 8080
//
// Input:   Argv[1] to contain the address, argv[2] to contain the port
// Output:
//*****************************************************************************
//
#if 0
int main( int argc, char *argv[] )
{
   int      spfd;                      // socket peer file descriptor
   int      efd;                       // epoll file descriptor
   struct   epoll_event    event;      // epoll event data structure
   struct   epoll_event    *events;
   bool     sendIdentifyMessage  = true;  // one time event flag

   if ( argc < 3 )
   {
      fprintf( stderr, "usage: tcp_client hostname port\n" );
      return 1;
   }

   initialization();

   spfd = createSocket( argv[1], argv[2] );
   if ( spfd < 0 )
   {
      printf( "Socket creation failure\n" );
      return 1;
   }
   currentServer.fd        = spfd;


   if ( (makeSocketNonBlocking( currentServer.fd )) == -1 )
   {
      perror( "SPFD NonBlocking" );
      return 1;
   }

   efd = epoll_create1(0);
   if ( efd == -1 )
   {
      perror( "epoll_create" );
      return 1;
   }

   // add peer connection to poll list
   event.data.fd  = currentServer.fd;
   event.events   = EPOLLIN;
   if ( (epoll_ctl ( efd, EPOLL_CTL_ADD, currentServer.fd, &event )) == -1 )
   {
      perror( "epoll_ctl" );
      return 1;
   }

   // add terminal connection to poll list
   event.data.fd  = fileno(stdin);
   event.events   = EPOLLIN;
   if ( (epoll_ctl ( efd, EPOLL_CTL_ADD, fileno(stdin), &event )) == -1 )
   {
      perror( "epoll_ctl" );
      return 1;
   }

   events = (epoll_event *)calloc ( MAXEVENTS, sizeof event );    // buffer for returned events

   printf( "Connected.\n" );
   printf( "To send data, enter text followed by enter.\n" );

   while ( 1 )
   {
      int            i                 = 0;
      int            eventCount        = 0;
      ssize_t  count;

      if ( timeToQuit )
      {
         break;      // exit main while loop to quit
      }

      // After initial connection send an identifying message to server
      // so it knows who this active client is.
      if ( sendIdentifyMessage )
      {
         sendIdentifyMessage = false;
         responseParser( HB_MSG, 0, &currentServer );
      }

            #if 0
               struct timeval timeout;
               timeout.tv_sec = 0;
               timeout.tv_usec = 100000;
               if ( select( socket_peer + 1, &reads, 0, 0, &timeout ) < 0 )
            #endif

      eventCount = epoll_wait( efd, events, MAXEVENTS, -1 );
      for ( i = 0; i < eventCount; i++ )
      {
         if (    ( events[i].events & EPOLLERR )      // error event
              || ( events[i].events & EPOLLHUP )      // hang up has occured
              || ( !(events[i].events & EPOLLIN ))    // read event has occured
            )
         {
            // socket error has occured or the socket is not ready for reading
            fprintf ( stderr, "epoll error\n" );
            close ( events[i].data.fd );
            continue;
         }
         else if ( currentServer.fd == events[i].data.fd )   // peer socket has an event
         {
            int done = 0;

            while ( 1 )
            {
               count = read( events[i].data.fd, readInBuffer, sizeof readInBuffer );

               if ( count == -1 )   // check error value
               {
                  if ( errno != EAGAIN )     // EAGAIN means we read all the available data, we are done
                  {
                     perror( "read" );
                     done = 1;
                  }
                  break;
               }
               else if ( count == 0 )
               {
                  done = 1;      // end of file, the remote has closed the connection
                  break;
               }

               printf("DBG: readInBuffer contents\n");    // KLM: debug
               for ( int k=0; k < 20; k++)
               {
                  printf("in[%d] %x\n", k, readInBuffer[k] );
               }

               if ( messageParser( readInBuffer, &currentServer ) < 0 )
               {
                  timeToQuit = true;
               }
            }  // end of read while loop

            if ( done )
            {
               ;  // do nothing for now
            }

         }
         else if ( fileno(stdin) == events[i].data.fd )  // terminal has an event
         {
            if ( !fgets( (char *)readInBuffer, sizeof readInBuffer, stdin ) )
               break;

            printf( "Sending: %s", readInBuffer );
            parseKeyboardInput( readInBuffer, strlen( (const char *)readInBuffer ), &currentServer );

                     #if 0
                        printf( "Sending: %s", readInBuffer );
                        int bytes_sent = send( currentServer.fd, readInBuffer, strlen( readInBuffer ), 0 );
                        printf( "Sent %d bytes.\n", bytes_sent );
                     #endif

         }
      }  // end of event for loop
   } //end while(1)

   printf( "Closing socket...\n" );
   close ( events[0].data.fd );
   free ( events );
   close ( currentServer.fd );

   printf( "Finished.\n" );
   return 0;
}
#endif




