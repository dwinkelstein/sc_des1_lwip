/*
 * calcuations.c
 *
 *  Created on: Feb 16, 2021
 *      Author: DanWinkelstein
 */


#include "SC_des1.h"
#include <math.h>

#define RMS_BADVALUE 999.9
#define PF_BADVALUE 1.0
#define FREQ_BADVALUE 99.9
#define PHASE_BADVALUE 720.0
#define NOISE_BADVALUE 999.9
#define V208_BADVALUE 999.9


// returns the RMS voltage or RMS current based on Mean Square ADC data, factor is the ADC conversion factor
double RMS_calculation(uint32_t reg_value, double factor){
	double results = (double) reg_value;
	results = sqrt(results);
	if(factor == 0) return -1;
	results = results/factor;
	if(results > 0 && results < 0.001) results = 0.001;
	if(results < 0 || results > RMS_BADVALUE) { results = -1;}
	return results;
}

// returns the PF based on Mean Square ADC data PF, voltage and current factor is the ADC conversion factor
double PF_calculation(double voltRMS, double currentRMS, uint32_t reg_value, double volt_factor, double current_factor){
	double totalPower = voltRMS * currentRMS;
	double pfFactor = volt_factor * current_factor;
	double reading = (double)reg_value;
	if(pfFactor == 0) return -1;
	double realPower = reading/pfFactor;
	if(totalPower == 0) return -1;
	double result = realPower/totalPower;
	if(result > 0 && result < 0.001) result = 0.001;
	if(result <0 || result > PF_BADVALUE)  // FIXME, data acquisition or calculation error
		return -1;
	else
		return result;
}

// frequency calculated based on 122.88MHz clock count, return is Hz
// hardware measures full-wave and averages
double FREQ_calculation(uint32_t reg_value){
	if(reg_value == 0) return -1;
	double results = FCLOCKS_PER_SECOND/((double) reg_value);
	if(results > 0 && results < 0.001) results = 0.001;
	if(results < 0 || results > FREQ_BADVALUE) { results = -1;}
	return results;
}

// phase calculated on 122.88MHz clock count return is degrees
// phase calculations are tricky since we are going from the rising edge of one phase to the falling edge
// of the other phase counted as 122.88 clock cycles
double PHASE_calculation(uint32_t freq_reg_value, uint32_t phase_reg_value){
	double rawPhase;
	if(freq_reg_value == 0) return -1;
	rawPhase = (double) phase_reg_value / (double) freq_reg_value * 360.0 + 180.0;
	if(rawPhase > 360.0){rawPhase -= 360.0;}
	if(rawPhase > 0 && rawPhase < 0.001) rawPhase = 0.001;
	if(rawPhase < 0 || rawPhase > PHASE_BADVALUE) { rawPhase = -1;}
	return rawPhase;

}

// noise is an unattached ADC input, register value is MS noise, return RMS noise
double NOISE_calculation(uint32_t reg_value){
	double results = sqrt((double) reg_value);
	if(results > 0 && results < 0.001) results = 0.001;
	if(results < 0 || results > NOISE_BADVALUE){ results = -1;}
	return results;
}

// calculate the phase-to-phase voltage based on phase-to-neutral RMS voltage and phase-to-phase offset
double VOLT208_calculation(double volt1, double volt2, double phase){
	double phase_rad = phase/360.0 * 2.0 * M_PI;
	double v2x = volt2 * cos(phase_rad);
	double v2y = volt2 * sin(phase_rad);
	double results = sqrt(pow((volt1 - v2x), 2) + pow(v2y,2));
	if(results > 0 && results < 0.001) results = 0.001;
	if(results < 0 || results > V208_BADVALUE){ results = -1;}
	return results;
}
