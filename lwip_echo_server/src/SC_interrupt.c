/*
 * SC_interrupt.c
 *
 *  Created on: Feb 18, 2021
 *      Author: DanWinkelstein
 */
#include "SC_des1.h"
extern 	SC_DES1_T	sc_des1_inst;


#define FABRIC_INTERRUPT_ID XPAR_FABRIC_IV_ALARM_MONITOR_V1_0_0_INTERRUPT_INTR
#define INTERRUPT_DEVICE_ID XPAR_SCUGIC_0_DEVICE_ID

extern VOLT_INT_EVENT_T voltage_interrupt_event;
extern FREQ_INT_EVENT_T frequency_interrupt_event;
extern KEYPAD_INTERRUPT_T keypad_interrupt;
extern CURRENT_INT_EVENT_T current_interrupt_event;
extern STREAM_INTERRUPT_T stream_interrupt;
extern uint32_t fabric_interrupt_event;
extern uint32_t stream_interrupt_status_reg;
extern VOLT_INTERRUPT_T VoltageStatus;
extern FREQ_INTERRUPT_T FreqStatus;
extern CURRENT_INTERRUPT_T CurrentStatus;



XScuGic xInterruptController;
XScuGic_Config *pxInterruptControllerConfig;

void fabric_interrupt_init(){

	Xil_ExceptionInit();
/* Initialize the interrupt controller driver. */
	pxInterruptControllerConfig = XScuGic_LookupConfig( INTERRUPT_DEVICE_ID );

	XScuGic_CfgInitialize( &xInterruptController,
			               pxInterruptControllerConfig,
						   pxInterruptControllerConfig->CpuBaseAddress);
/* Connect the interrupt controller interrupt handler to the hardware */
/* interrupt handling logic in the ARM processor. */
	Xil_ExceptionRegisterHandler( XIL_EXCEPTION_ID_IRQ_INT,
                               ( Xil_ExceptionHandler ) XScuGic_InterruptHandler,
                               &xInterruptController);

/*now connect your handler*/
	XScuGic_Connect(&xInterruptController, FABRIC_INTERRUPT_ID,
			        (Xil_InterruptHandler) fabric_interrupt_handler, NULL);

	XScuGic_Enable(&xInterruptController, FABRIC_INTERRUPT_ID);
//OPTIONAL
	//XScuGic_InterruptMaptoCpu(GICInstance, XPAR_CPU_ID, irqNo);
	//XScuGic_SetPriorityTriggerType(GICInstance, irqNo, (configMAX_API_CALL_INTERRUPT_PRIORITY+1)<<3, 3);

/* Enable interrupts in the ARM. */
	Xil_ExceptionEnable();

}

void fabric_interrupt_handler(void *CallBackRef, u32 Event){

	voltage_interrupt_event.reg = Xil_In32(VOLT_INT_EVENT_ADDR);
	if(voltage_interrupt_event.reg){
		VoltageStatus.reg = Xil_In32(VOLT_INTERRUPT_ADDR);
	}
	frequency_interrupt_event.reg = Xil_In32(FREQ_INT_EVENT_ADDR);
	if(frequency_interrupt_event.reg){
		FreqStatus.reg = Xil_In32(FREQ_INTERRUPT_ADDR);
	}
	current_interrupt_event.reg = Xil_In32(CURRENT_INT_EVENT_ADDR);
	if(current_interrupt_event.reg){
		CurrentStatus.reg = Xil_In32(CURRENT_INTERRUPT_ADDR);
	}
	keypad_interrupt.reg = Xil_In32(KEYPAD_INTERRUPT_ADDR);
	stream_interrupt.reg = Xil_In32(STREAM_INTERRUPT_ADDR);
//	stream_interrupt_status_reg = Xil_In32(FIFO_REG_ISR_ADDR);
//	Xil_Out32(FIFO_REG_ISR_ADDR,0xFFF80000); //clear the FIFO ISR register <- move to callback

	// the following allows us to debug the main service routine
	CONTROL_REG_T local_control_reg;
	local_control_reg.reg = Xil_In32(CONTROL_REG_ADDR);
	local_control_reg.bits.interruptEnable = 0;
	sc_des1_inst.alarm_data->control_reg.reg = local_control_reg.reg;
	Xil_Out32(CONTROL_REG_ADDR, local_control_reg.reg);

	fabric_interrupt_event = 1;

}
