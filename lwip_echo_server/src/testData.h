/*
 * Copyright (c) 2021 Aura Technologies, LLC
 *
 */

#ifndef TAC_PWR_TESTDATA_H
#define TAC_PWR_TESTDATA_H


   #define SINGLE_CYCLE_BLOCKS          8    // number of single cycles sets in snapshot data
   #define REALTIME_BLOCKS              3    // number of realtime data blocks


   typedef struct {                 // used to hold limits from the controller sent as u32_ts
      u32_t   main_l1_volt_max;
      u32_t   main_l1_volt_min;
      u32_t   main_l2_volt_max;
      u32_t   main_l2_volt_min;
      u32_t   main_l3_volt_max;
      u32_t   main_l3_volt_min;
      u32_t   main_freq_max;
      u32_t   main_freq_min;
      u32_t   main_phase_max;
      u32_t   main_phase_min;
      u32_t   main_l1_amp_max;
      u32_t   main_l2_amp_max;
      u32_t   main_l3_amp_max;
      u32_t   main_n_amp_max;
      u32_t   f60a_l1_amp_max;
      u32_t   f60a_l2_amp_max;
      u32_t   f60a_l3_amp_max;
      u32_t   f60a_n_amp_max;
      u32_t   f60b_l1_amp_max;
      u32_t   f60b_l2_amp_max;
      u32_t   f60b_l3_amp_max;
      u32_t   f60b_n_amp_max;
      u32_t   f40a_l1_amp_max;
      u32_t   f40a_l2_amp_max;
      u32_t   f40a_l3_amp_max;
      u32_t   f40a_n_amp_max;
      u32_t   f40b_l1_amp_max;
      u32_t   f40b_l2_amp_max;
      u32_t   f40b_l3_amp_max;
      u32_t   f40b_n_amp_max;
      u32_t   f20a_l1_amp_max;
      u32_t   f20a_n_amp_max;
      u32_t   f20b_l2_amp_max;
      u32_t   f20b_n_amp_max;
      u32_t   fbyp_l1_amp_max;
      u32_t   fbyp_l2_amp_max;
      u32_t   fbyp_l3_amp_max;
      u32_t   fbyp_n_amp_max;
      u32_t   endOfStruct;
   } newAlarmLimits;

   typedef struct {                 // used for testing and sending to controller as longs
      u32_t     main_l1_volt_max;
      u32_t     main_l1_volt_min;
      u32_t     main_l2_volt_max;
      u32_t     main_l2_volt_min;
      u32_t     main_l3_volt_max;
      u32_t     main_l3_volt_min;
      u32_t     main_freq_max;
      u32_t     main_freq_min;
      u32_t     main_phase_max;
      u32_t     main_phase_min;
      u32_t     main_l1_amp_max;
      u32_t     main_l2_amp_max;
      u32_t     main_l3_amp_max;
      u32_t     main_n_amp_max;
      u32_t     f60a_l1_amp_max;
      u32_t     f60a_l2_amp_max;
      u32_t     f60a_l3_amp_max;
      u32_t     f60a_n_amp_max;
      u32_t     f60b_l1_amp_max;
      u32_t     f60b_l2_amp_max;
      u32_t     f60b_l3_amp_max;
      u32_t     f60b_n_amp_max;
      u32_t     f40a_l1_amp_max;
      u32_t     f40a_l2_amp_max;
      u32_t     f40a_l3_amp_max;
      u32_t     f40a_n_amp_max;
      u32_t     f40b_l1_amp_max;
      u32_t     f40b_l2_amp_max;
      u32_t     f40b_l3_amp_max;
      u32_t     f40b_n_amp_max;
      u32_t     f20a_l1_amp_max;
      u32_t     f20a_n_amp_max;
      u32_t     f20b_l2_amp_max;
      u32_t     f20b_n_amp_max;
      u32_t     fbyp_l1_amp_max;
      u32_t     fbyp_l2_amp_max;
      u32_t     fbyp_l3_amp_max;
      u32_t     fbyp_n_amp_max;
      u32_t     endOfStruct;
   } alarmLimits;

   // test data alarm limits - for development only
   static alarmLimits testAlarmLimits = {
      250,   // main_l1_volt_max;
      250,   // main_l1_volt_min;
      250,   // main_l2_volt_max;
      250,   // main_l2_volt_min;
      250,   // main_l3_volt_max;
      250,   // main_l3_volt_min;
      250,   // main_freq_max;
      250,   // main_freq_min;
      250,   // main_phase_max;
      250,   // main_phase_min;
      250,   // main_l1_amp_max;
      250,   // main_l2_amp_max;
      250,   // main_l3_amp_max;
      250,   // main_n_amp_max;
      250,   // f60a_l1_amp_max;
      250,   // f60a_l2_amp_max;
      250,   // f60a_l3_amp_max;
      250,   // f60a_n_amp_max;
      250,   // f60b_l1_amp_max;
      250,   // f60b_l2_amp_max;
      250,   // f60b_l3_amp_max;
      250,   // f60b_n_amp_max;
      250,   // f40a_l1_amp_max;
      250,   // f40a_l2_amp_max;
      250,   // f40a_l3_amp_max;
      250,   // f40a_n_amp_max;
      250,   // f40b_l1_amp_max;
      250,   // f40b_l2_amp_max;
      250,   // f40b_l3_amp_max;
      250,   // f40b_n_amp_max;
      250,   // f20a_l1_amp_max;
      250,   // f20a_n_amp_max;
      250,   // f20b_l2_amp_max;
      200,   // f20b_n_amp_max;
      195,   // fbyp_l1_amp_max;
      175,   // fbyp_l2_amp_max;
      150,   // fbyp_l3_amp_max;
      125,   // fbyp_n_amp_max;
        0
   };

   typedef struct {
      unsigned char  main_60A_priority;
      unsigned char  main_60B_priority;
      unsigned char  main_40A_priority;
      unsigned char  main_40B_priority;
      unsigned char  main_20A_priority;
      unsigned char  main_20B_priority;
      unsigned char  endOfStruct;
   } feedShedPriority;

   static feedShedPriority    testFeedShedPriority = {
      127,  // main_60A_priority;
      127,  // main_60B_priority;
      127,  // main_40A_priority;
      127,  // main_40B_priority;
      127,  // main_20A_priority;
      127,  // main_20B_priority;
        0
   };

   typedef struct {
      double   voltage_factor;
      double   current_main_factor;
      double   current_60a_factor;
      double   current_60b_factor;
      double   current_40a_factor;
      double   current_40b_factor;
      double   current_20a_factor;
      double   current_20b_factor;
      double   current_bypass_factor;
      double   endOfStruct;
   } correctionFactors;

   // test data correction factors - for development only
   static correctionFactors   testCorrectionFactors = {
      127.5,   // voltage_factor;
      127.5,   // current_main_factor;
      127.5,   // current_60a_factor;
      127.5,   // current_60b_factor;
      127.5,   // current_40a_factor;
      127.5,   // current_40b_factor;
      127.5,   // current_20a_factor;
      127.5,   // current_20b_factor;
      127.5,   // current_bypass_factor;
       0.0
   };

   typedef struct {
      u32_t  timeHigh;
      u32_t  timeLow;
      u32_t  timeNano;
      u32_t  endOfStruct;
   } timeTest;

   static timeTest   testTimeValue = {
      999888,     //timeHigh;
      777666,     //timeLow;
      555444,     //timeNano;
      0
   };


   typedef struct {
      u32_t     main_l1_volt_reg;
      u32_t     main_l2_volt_reg;
      u32_t     main_l3_volt_reg;
      u32_t     main_l1_freq_reg;
      u32_t     main_l2_freq_reg;
      u32_t     main_l3_freq_reg;
      u32_t     main_l1_phase_reg;
      u32_t     main_l2_phase_reg;
      u32_t     main_l3_phase_reg;
      u32_t     main_l1_amp_reg;
      u32_t     main_l2_amp_reg;
      u32_t     main_l3_amp_reg;
      u32_t     main_n_amp_reg;
      u32_t     f60a_l1_amp_reg;
      u32_t     f60a_l2_amp_reg;
      u32_t     f60a_l3_amp_reg;
      u32_t     f60a_n_amp_reg;
      u32_t     f60b_l1_amp_reg;
      u32_t     f60b_l2_amp_reg;
      u32_t     f60b_l3_amp_reg;
      u32_t     f60b_n_amp_reg;
      u32_t     f40a_l1_amp_reg;
      u32_t     f40a_l2_amp_reg;
      u32_t     f40a_l3_amp_reg;
      u32_t     f40a_n_amp_reg;
      u32_t     f40b_l1_amp_reg;
      u32_t     f40b_l2_amp_reg;
      u32_t     f40b_l3_amp_reg;
      u32_t     f40b_n_amp_reg;
      u32_t     f20a_l1_amp_reg;
      u32_t     f20a_n_amp_reg;
      u32_t     f20b_l2_amp_reg;
      u32_t     f20b_n_amp_reg;
      u32_t     fbyp_l1_amp_reg;
      u32_t     fbyp_l2_amp_reg;
      u32_t     fbyp_l3_amp_reg;
      u32_t     fbyp_n_amp_reg;

      u32_t     timeHigh;      // seconds_reg[63:32] bits 63:55 = FF timestamp data ID
      u32_t     timeLow;       // seconds_reg[31:0] time since epoc in seconds use date + "%s"
      u32_t     timeNano;      // nanoseconds_reg; 0-999,999,999 decimal use date + "%N"
      unsigned char     endOfBlock;
   } cycleDataBlock;

   typedef struct {
      u32_t     controlReg;
      u32_t     contactorState;

      double            main_l1_volt;
      double            main_l2_volt;
      double            main_l3_volt;
      double            main_l1_freq;
      double            main_l2_freq;
      double            main_l3_freq;
      double            main_l1_l2_phase;
      double            main_l2_l3_phase;
      double            main_l3_l1_phase;
      double            main_l1_amp;
      double            main_l2_amp;
      double            main_l3_amp;
      double            main_n_amp;
      double            f60a_l1_amp;
      double            f60a_l2_amp;
      double            f60a_l3_amp;
      double            f60a_n_amp;
      double            f60b_l1_amp;
      double            f60b_l2_amp;
      double            f60b_l3_amp;
      double            f60b_n_amp;
      double            f40a_l1_amp;
      double            f40a_l2_amp;
      double            f40a_l3_amp;
      double            f40a_n_amp;
      double            f40b_l1_amp;
      double            f40b_l2_amp;
      double            f40b_l3_amp;
      double            f40b_n_amp;
      double            f20a_l1_amp;
      double            f20a_n_amp;
      double            f20b_l2_amp;
      double            f20b_n_amp;
      double            fbyp_l1_amp;
      double            fbyp_l2_amp;
      double            fbyp_l3_amp;
      double            fbyp_n_amp;

      u32_t     timeHigh;      // seconds_reg[63:32] bits 63:55 = FF timestamp data ID
      u32_t     timeLow;       // seconds_reg[31:0] time since epoc in seconds use date + "%s"
      u32_t     timeNano;      // nanoseconds_reg; 0-999,999,999 decimal use date + "%N"
      u32_t     endSnapData;

      cycleDataBlock    cycleData[SINGLE_CYCLE_BLOCKS];  // raw cycle data used to make the snapshot data
   } snapshotData;

   static snapshotData  testSnapshotData = {
      10, 20, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 10.0, 20.0, 30.0, 100.0, 100.0, 100.0, 100.0, 60.0,
      60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 40.0, 20.0,
      20.0, 20.0, 20.0, 60.0, 60.0, 60.0, 60.0, 111, 222, 333, 0,
      { { 51, 51, 51, 41, 41, 41, 61, 61, 61, 91, 91, 91, 91, 71, 71, 71, 71, 81, 81, 81, 81, 41, 41,
          41, 41, 31, 31, 31, 31, 21, 21, 21, 21, 11, 11, 11, 11, 111, 555, 999, 0
        },
        { 52, 52, 52, 42, 42, 42, 62, 62, 62, 92, 92, 92, 92, 72, 72, 72, 72, 82, 82, 82, 82, 42, 42,
          42, 42, 32, 32, 32, 32, 22, 22, 22, 22, 12, 12, 12, 12, 222, 555, 999, 0
        },
        { 53, 53, 53, 43, 43, 43, 63, 63, 63, 93, 93, 93, 93, 73, 73, 73, 73, 83, 83, 83, 83, 43, 43,
          43, 43, 33, 33, 33, 33, 23, 23, 23, 23, 13, 13, 13, 13, 333, 555, 999, 0
        },
        { 54, 54, 54, 44, 44, 44, 64, 64, 64, 94, 94, 94, 94, 74, 74, 74, 74, 84, 84, 84, 84, 44, 44,
          44, 44, 34, 34, 34, 34, 24, 24, 24, 24, 14, 14, 14, 14, 444, 555, 999, 0
        },
        { 75, 75, 75, 45, 45, 45, 65, 65, 65, 95, 95, 95, 95, 75, 75, 75, 75, 85, 85, 85, 85, 45, 45,
          45, 45, 35, 35, 35, 35, 25, 25, 25, 25, 15, 15, 15, 15, 555, 555, 999, 0
        },
        { 76, 76, 76, 46, 46, 46, 66, 66, 66, 96, 96, 96, 96, 76, 76, 76, 76, 86, 86, 86, 86, 46, 46,
          46, 46, 36, 36, 36, 36, 26, 26, 26, 26, 16, 16, 16, 16, 666, 555, 999, 0
        },
        { 67, 67, 67, 47, 47, 47, 67, 67, 67, 97, 97, 97, 97, 77, 77, 77, 77, 87, 87, 87, 87, 47, 47,
          47, 47, 37, 37, 37, 37, 27, 27, 27, 27, 17, 17, 17, 17, 777, 555, 999, 0
        },
        { 68, 68, 68, 48, 48, 48, 68, 68, 68, 98, 98, 98, 98, 78, 78, 78, 78, 88, 88, 88, 88, 48, 48,
          48, 48, 38, 38, 38, 38, 28, 28, 28, 28, 18, 18, 18, 18, 888, 555, 999, 0
        }
      }
   };

   typedef struct {
         u32_t     voltage_l1;
         u32_t     voltage_l2;
         u32_t     voltage_l3;
         u32_t     noise;
         u32_t     main_l1_amp;
         u32_t     main_l2_amp;
         u32_t     main_l3_amp;
         u32_t     main_n_amp;
         u32_t     f60a_l1_amp;
         u32_t     f60a_l2_amp;
         u32_t     f60a_l3_amp;
         u32_t     f60a_n_amp;
         u32_t     f60b_l1_amp;
         u32_t     f60b_l2_amp;
         u32_t     f60b_l3_amp;
         u32_t     f60b_n_amp;
         u32_t     f40a_l1_amp;
         u32_t     f40a_l2_amp;
         u32_t     f40a_l3_amp;
         u32_t     f40a_n_amp;
         u32_t     f40b_l1_amp;
         u32_t     f40b_l2_amp;
         u32_t     f40b_l3_amp;
         u32_t     f40b_n_amp;
         u32_t     f20a_l1_amp;
         u32_t     f20a_n_amp;
         u32_t     f20b_l2_amp;
         u32_t     f20b_n_amp;
         u32_t     fbyp_l1_amp;
         u32_t     fbyp_l2_amp;
         u32_t     fbyp_l3_amp;
         u32_t     fbyp_n_amp;
         u32_t     transitionValue;  // unused "DEADBEEF" value for output organization in IPDU client

         u32_t     timeHigh;      // seconds_reg[63:32] bits 63:55 = FF timestamp data ID
         u32_t     timeLow;       // seconds_reg[31:0] time since epoc in seconds use date + "%s"
         u32_t     timeNano;      // nanoseconds_reg; 0-999,999,999 decimal use date + "%N"
         u32_t     endRealtimeData;
   } realtimeData;

   static realtimeData  testRealtimeData[REALTIME_BLOCKS] = {
      { 65, 65, 65, 5, 115, 115, 115, 115, 75, 75, 75, 75, 75, 75, 75, 75, 35, 35, 35, 35, 35, 35, 35,
        35, 35, 35, 35, 35, 95, 95, 95, 95, 0xDEADBEEF, 555, 444, 333, 0
      },
      { 63, 63, 63, 3, 113, 113, 113, 113, 73, 73, 73, 73, 73, 73, 73, 73, 33, 33, 33, 33, 33, 33, 33,
        33, 33, 33, 33, 33, 93, 93, 93, 93, 0xDEADBEEF, 333, 444, 555, 0
      },
      { 60, 60, 60, 10, 110, 110, 110, 110, 70, 70, 70, 70, 70, 70, 70, 70, 30, 30, 30, 30, 30, 30, 30,
        30, 30, 30, 30, 30, 90, 90, 90, 90, 0xDEADBEEF, 999, 333, 666, 0
      }
   };

#endif   // TAC_PWR_TESTDATA_H
